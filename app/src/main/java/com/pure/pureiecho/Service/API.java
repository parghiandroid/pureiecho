package com.pure.pureiecho.Service;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pure.pureiecho.Global.Global;
import com.pure.pureiecho.Global.SendMail;
import com.pure.pureiecho.Global.StaticUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class API {

    static JSONResponse jsonResponse;

    public static void APICalling(final Context mContext, final Fragment fragment, final String name,
                                  final String APIType, String strURL, String key[],
                                  String value[], final HashMap<String, String> headers) {
        String url = StaticUtility.URL + strURL;
        JSONObject jsonObject = Global.bodyParameter(key, value);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null) {
                            try {
                                if (fragment != null) {
                                    jsonResponse = (JSONResponse) fragment;
                                    jsonResponse.JsonAPIResponse(APIType, response);
                                } else {
                                    jsonResponse = (JSONResponse) mContext;
                                    jsonResponse.JsonAPIResponse(APIType, response);
                                }

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                //Creating SendMail object
                                SendMail sm = new SendMail(mContext, StaticUtility.TO_EMAIL, StaticUtility.SUBJECT,
                                        "Getting error in " + name + " When json parsing " + e.toString());
                                //Executing sendmail to send email
                                sm.execute();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json = null;
                JSONObject jsonObject = null;
                if (error.networkResponse != null) {
                    try {
                        json = new String(error.networkResponse.data,
                                HttpHeaderParser.parseCharset(error.networkResponse.headers));

                        jsonObject = new JSONObject(json);
                        if (fragment != null) {
                            jsonResponse = (JSONResponse) fragment;
                            jsonResponse.JsonAPIResponse(APIType, jsonObject);
                        } else {
                            jsonResponse = (JSONResponse) mContext;
                            jsonResponse.JsonAPIResponse(APIType, jsonObject);
                        }
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    //Creating SendMail object
                    SendMail sm = new SendMail(mContext, StaticUtility.TO_EMAIL, StaticUtility.SUBJECT,
                            "Getting error in" + name + "When json parsing" + error.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "json_array_req");
    }

    public static void APICallingJson(final Context mContext, final Fragment fragment, final String name,
                                  final String APIType, String strURL, JSONObject jsonObject, final HashMap<String, String> headers) {
        String url = StaticUtility.URL + strURL;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null) {
                            try {
                                if (fragment != null) {
                                    jsonResponse = (JSONResponse) fragment;
                                    jsonResponse.JsonAPIResponse(APIType, response);
                                } else {
                                    jsonResponse = (JSONResponse) mContext;
                                    jsonResponse.JsonAPIResponse(APIType, response);
                                }

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                //Creating SendMail object
                                SendMail sm = new SendMail(mContext, StaticUtility.TO_EMAIL, StaticUtility.SUBJECT,
                                        "Getting error in " + name + " When json parsing " + e.toString());
                                //Executing sendmail to send email
                                sm.execute();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json = null;
                JSONObject jsonObject = null;
                if (error.networkResponse != null) {
                    try {
                        json = new String(error.networkResponse.data,
                                HttpHeaderParser.parseCharset(error.networkResponse.headers));

                        jsonObject = new JSONObject(json);
                        if (fragment != null) {
                            jsonResponse = (JSONResponse) fragment;
                            jsonResponse.JsonAPIResponse(APIType, jsonObject);
                        } else {
                            jsonResponse = (JSONResponse) mContext;
                            jsonResponse.JsonAPIResponse(APIType, jsonObject);
                        }
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    //Creating SendMail object
                    SendMail sm = new SendMail(mContext, StaticUtility.TO_EMAIL, StaticUtility.SUBJECT,
                            "Getting error in " + name + " When json parsing " + error.toString());
                    //Executing sendmail to send email
                    sm.execute();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "json_array_req");
    }

    public interface JSONResponse {
        void JsonAPIResponse(String strAPIType, JSONObject response);
    }
}