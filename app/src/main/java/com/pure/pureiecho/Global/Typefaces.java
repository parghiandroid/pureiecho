package com.pure.pureiecho.Global;

import android.content.Context;
import android.graphics.Typeface;

public class Typefaces {

    public static Typeface Snippet(Context context) {
        return Typeface.createFromAsset(context.getAssets(),
                "Fonts/Snippet.ttf");
    }

    public static Typeface Ubuntu_Regular(Context context) {
        return Typeface.createFromAsset(context.getAssets(),
                "Fonts/Ubuntu-R.ttf");
    }
}
