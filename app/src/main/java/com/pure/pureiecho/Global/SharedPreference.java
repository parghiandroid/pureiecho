package com.pure.pureiecho.Global;

import android.content.Context;

public class SharedPreference {
    private static android.content.SharedPreferences preferences;
    private static android.content.SharedPreferences.Editor editor;


    //region Shared Preference
    public static void CreatePreference(Context context, String preferenceName) {
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
    }
    //endregion

    public static void SavePreference(String preferenceKey, String preferenceValue) {
        editor.putString(preferenceKey, preferenceValue);
        editor.apply();
    }

    public static String GetPreference(Context context, String preferenceName, String preferenceKey) {
        String text;
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        text = preferences.getString(preferenceKey, null);
        return text;
    }

    public static void ClearPreference(Context context, String preferenceName) {
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public static void RemovePreference(Context context, String preferenceName, String preferenceKey) {
        preferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.remove(preferenceKey);
        editor.apply();
    }
}
