package com.pure.pureiecho.Model;

import org.json.JSONArray;
import org.json.JSONObject;

public class Answer {

    private String mtext, mimage;

    private JSONArray mxscale, myscale;

    private JSONObject mobject;

    public Answer() {
    }

    public Answer(String mtext, String mimage) {
        this.mtext = mtext;
        this.mimage = mimage;
    }

    public Answer(String text) {
        this.mtext = text;
    }

    public Answer(JSONObject object) {
        this.mobject = object;
    }

    public Answer(JSONArray xscale, JSONArray yscale){
        this.mxscale = xscale;
        this.myscale = yscale;
    }

    public JSONArray getMxscale() {
        return mxscale;
    }

    public JSONObject getMobject() {
        return mobject;
    }

    public JSONArray getMyscale() {
        return myscale;
    }

    public String getMtext() {
        return mtext;
    }

    public String getMimage() {
        return mimage;
    }

    public void setMimage(String mimage) {
        this.mimage = mimage;
    }
}
