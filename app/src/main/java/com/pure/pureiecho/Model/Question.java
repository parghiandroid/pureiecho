package com.pure.pureiecho.Model;

import java.util.ArrayList;

public class Question {
    String question_id;
    String question;
    String is_mandatory;
    String strQuestion_image_ans;
    String ans_display_type;
    private String mquestion_image;
    ArrayList<Answer> Options;
    String ans_values;

    public Question(String question_id, String question, String question_image, String strQuestion_image_ans, String is_mandatory, String ans_display_type, ArrayList<Answer> options) {
        this.question_id = question_id;
        this.question = question;
        this.is_mandatory = is_mandatory;
        this.ans_display_type = ans_display_type;
        this.mquestion_image = question_image;
        this.strQuestion_image_ans = strQuestion_image_ans;
        Options = options;
    }

    public Question(String question_id, String question, String question_image, String strQuestion_image_ans, String is_mandatory, String ans_display_type, String ans_values) {
        this.question_id = question_id;
        this.question = question;
        this.is_mandatory = is_mandatory;
        this.ans_display_type = ans_display_type;
        this.mquestion_image = question_image;
        this.strQuestion_image_ans = strQuestion_image_ans;
        this.ans_values = ans_values;
    }

    public String getStrQuestion_image_ans() {
        return strQuestion_image_ans;
    }

    public String getAns_values() {
        return ans_values;
    }

    public String getMquestion_image() {
        return mquestion_image;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public String getQuestion() {
        return question;
    }

    public String getIs_mandatory() {
        return is_mandatory;
    }

    public String getAns_display_type() {
        return ans_display_type;
    }

    public ArrayList<Answer> getOptions() {
        return Options;
    }
}
