package com.pure.pureiecho.Model;

public class Survey {
    String survey_id;
    String encode_survey_id;
    String title;
    private String mApp_id, mApp_secret;

    public Survey(String survey_id, String encode_survey_id, String title, String app_id, String app_secret) {
        this.survey_id = survey_id;
        this.encode_survey_id = encode_survey_id;
        this.title = title;
        this.mApp_id = app_id;
        this.mApp_secret = app_secret;
    }

    public String getEncode_survey_id() {
        return encode_survey_id;
    }

    public String getSurvey_id() {
        return survey_id;
    }

    public String getTitle() {
        return title;
    }

    public String getmApp_id() {
        return mApp_id;
    }

    public String getmApp_secret() {
        return mApp_secret;
    }
}
