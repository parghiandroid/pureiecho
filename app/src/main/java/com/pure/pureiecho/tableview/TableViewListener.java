/*
 * Copyright (c) 2018. Evren Coşkun
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.pure.pureiecho.tableview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.pure.pureiecho.Activity.QuestionnaireScreen;
import com.pure.pureiecho.tableview.model.Cell;

import java.util.List;

/**
 * Created by evrencoskun on 21/09/2017.
 */

public class TableViewListener implements ITableViewListener {

    private Toast mToast;
    private Context mContext;
    private TableView mTableView;
    private List<List<Cell>> mcelllist;

    public TableViewListener(TableView tableView, List<List<Cell>> celllist) {
        this.mContext = tableView.getContext();
        this.mTableView = tableView;
        this.mcelllist = celllist;
    }

    /**
     * Called when user click any cell item.
     *
     * @param cellView : Clicked Cell ViewHolder.
     * @param column   : X (Column) position of Clicked Cell item.
     * @param row      : Y (Row) position of Clicked Cell item.
     */
    @Override
    public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

        // Do what you want.
        //showToast("Cell " + column + " " + row + " has been clicked.");

    }

    /**
     * Called when user long press any cell item.
     *
     * @param cellView : Long Pressed Cell ViewHolder.
     * @param column   : X (Column) position of Long Pressed Cell item.
     * @param row      : Y (Row) position of Long Pressed Cell item.
     */
    @Override
    public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, final int column,
                                  int row) {
        // Do What you want
        if( mTableView.getAdapter().getCelltype() == 1) {
            for (int i = 0; i < mcelllist.size(); i++) {
                List<Cell> rowcell = mcelllist.get(i);
                for (int j = 0; j < rowcell.size(); j++) {
                    Cell cell = rowcell.get(j);
                    if (i == row && j == column) {
                        cell.setMischecked(true);
                        QuestionnaireScreen.multipoint[row] = cell.getData().toString();
                        mTableView.getAdapter().changeCellItem(j, i, cell);
                    } else if (i == row) {
                        cell.setMischecked(false);
                        mTableView.getAdapter().changeCellItem(j, i, cell);
                    }
                }
            }
        }else {
            for (int i = 0; i < mcelllist.size(); i++) {
                List<Cell> rowcell = mcelllist.get(i);
                for (int j = 0; j < rowcell.size(); j++) {
                    Cell cell = rowcell.get(j);
                    if (i == row && j == column) {
                        if(cell.isMischecked()) {
                            cell.setMischecked(false);
                            QuestionnaireScreen.multiselect[row][column] = "";
                            mTableView.getAdapter().changeCellItem(j, i, cell);
                        }else {
                            cell.setMischecked(true);
                            QuestionnaireScreen.multiselect[row][column] = cell.getData().toString();
                            mTableView.getAdapter().changeCellItem(j, i, cell);
                        }
                    }
                }
            }
        }

        //showToast("Cell " + column + " " + row + " has been long pressed.");
        /*QuestionnaireScreen questionnaireScreen = new QuestionnaireScreen();
        questionnaireScreen.setdatachanged(row, column);*/
    }

    /**
     * Called when user click any column header item.
     *
     * @param columnHeaderView : Clicked Column Header ViewHolder.
     * @param column           : X (Column) position of Clicked Column Header item.
     */
    @Override
    public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int
            column) {
        // Do what you want.
        //showToast("Column header  " + column + " has been clicked.");
    }

    /**
     * Called when user long press any column header item.
     *
     * @param columnHeaderView : Long Pressed Column Header ViewHolder.
     * @param column           : X (Column) position of Long Pressed Column Header item.
     */
    @Override
    public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int
            column) {


    }

    /**
     * Called when user click any Row Header item.
     *
     * @param rowHeaderView : Clicked Row Header ViewHolder.
     * @param row           : Y (Row) position of Clicked Row Header item.
     */
    @Override
    public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
        // Do what you want.


        //showToast("Row header " + row + " has been clicked.");
    }

    /**
     * Called when user long press any row header item.
     *
     * @param rowHeaderView : Long Pressed Row Header ViewHolder.
     * @param row           : Y (Row) position of Long Pressed Row Header item.
     */
    @Override
    public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {


    }


    private void showToast(String p_strMessage) {
        if (mToast == null) {
            mToast = Toast.makeText(mContext, "", Toast.LENGTH_SHORT);
        }

        mToast.setText(p_strMessage);
        mToast.show();
    }

}
