/*
 * Copyright (c) 2018. Evren Coşkun
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.pure.pureiecho.tableview.holder;

import android.provider.MediaStore;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;
import com.pure.pureiecho.R;
import com.pure.pureiecho.tableview.model.Cell;

public class CellViewHolder extends AbstractViewHolder {

    public final RadioButton radio_cell;
    public final CheckBox checkbox_cell;
    public final LinearLayout cell_container;
    private Cell cell;

    public CellViewHolder(View itemView) {
        super(itemView);
        radio_cell =  itemView.findViewById(R.id.radio_cell);
        checkbox_cell =  itemView.findViewById(R.id.checkbox_cell);
        cell_container =  itemView.findViewById(R.id.cell_container);
    }

    public void setCell(Cell cell, int POINT_SCALE_CELL_TYPE) {
        this.cell = cell;
        if(POINT_SCALE_CELL_TYPE == 1) {
            checkbox_cell.setVisibility(View.GONE);
            if (this.cell.isMischecked()) {
                radio_cell.setChecked(true);
            } else {
                radio_cell.setChecked(false);
            }
        }else {
            radio_cell.setVisibility(View.GONE);
            if (this.cell.isMischecked()) {
                checkbox_cell.setChecked(true);
            }else {
                checkbox_cell.setChecked(false);
            }
        }

        // If your TableView should have auto resize for cells & columns.
        // Then you should consider the below lines. Otherwise, you can ignore them.

        // It is necessary to remeasure itself.
        cell_container.getLayoutParams().width = LinearLayout.LayoutParams.WRAP_CONTENT;
    }
}