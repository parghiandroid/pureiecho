/*
 * Copyright (c) 2018. Evren Coşkun
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.pure.pureiecho.tableview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.pure.pureiecho.R;
import com.pure.pureiecho.tableview.model.Cell;
import com.pure.pureiecho.tableview.model.ColumnHeader;
import com.pure.pureiecho.tableview.model.RowHeader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by evrencoskun on 4.02.2018.
 */

public class TableViewModel {

    // Columns indexes
    public static final int MOOD_COLUMN_INDEX = 3;
    public static final int GENDER_COLUMN_INDEX = 4;

    // Constant values for icons
    public static final int SAD = 1;
    public static final int HAPPY = 2;
    public static final int BOY = 1;
    public static final int GIRL = 2;

    // Constant size for dummy data sets
    private static final int COLUMN_SIZE = 500;
    private static final int ROW_SIZE = 500;


    private Context mContext;

    public TableViewModel(Context context) {
        mContext = context;

    }

    private List<RowHeader> getSimpleRowHeaderList(JSONArray rowheader) {
        List<RowHeader> list = new ArrayList<>();
        for (int i = 0; i < rowheader.length(); i++) {
            RowHeader header = new RowHeader(String.valueOf(i), rowheader.opt(i).toString(), false);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    public static List<RowHeader> getSimpleRowHeaderList(int startIndex) {
        List<RowHeader> list = new ArrayList<>();
        for (int i = 0; i < ROW_SIZE; i++) {
            RowHeader header = new RowHeader(String.valueOf(i), "row " + (startIndex + i), false);
            list.add(header);
        }

        return list;
    }


    private List<ColumnHeader> getSimpleColumnHeaderList() {
        List<ColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < COLUMN_SIZE; i++) {
            String title = "column " + i;
            if (i % 6 == 2) {
                title = "large column " + i;
            } else if (i == MOOD_COLUMN_INDEX) {
                title = "mood";
            } else if (i == GENDER_COLUMN_INDEX) {
                title = "gender";
            }
            ColumnHeader header = new ColumnHeader(String.valueOf(i), title, false);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<ColumnHeader> getRandomColumnHeaderList(JSONArray columnheader) {
        List<ColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < columnheader.length(); i++) {
            ColumnHeader header = new ColumnHeader(String.valueOf(i), columnheader.opt(i).toString(), false);
            list.add(header);
        }

        return list;
    }

    private List<List<Cell>> getSimpleCellList() {
        List<List<Cell>> list = new ArrayList<>();
        for (int i = 0; i < ROW_SIZE; i++) {
            List<Cell> cellList = new ArrayList<>();

            for (int j = 0; j < COLUMN_SIZE; j++) {
                String text = "cell " + j + " " + i;
                if (j % 4 == 0 && i % 5 == 0) {
                    text = "large cell " + j + " " + i + ".";
                }
                String id = j + "-" + i;

                Cell cell = new Cell(id, text, false);
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<Cell>> getCellListForSortingTest(JSONArray yscale, JSONArray xscale, JSONArray jaAns) {
        List<List<Cell>> list = new ArrayList<>();
        for (int i = 0; i < yscale.length(); i++) {
            List<Cell> cellList = new ArrayList<>();
            for (int j = 0; j < xscale.length(); j++) {
                Object text = xscale.opt(j);

                String id = String.valueOf(i) + "-" + String.valueOf(j);
                boolean isChecked = false;
                if (jaAns != null && jaAns.length() > 0) {
                    /*for (int k = 0; k < jaAns.length(); k++) {*/
                    try {
                        /*if (jaAns.get(i) instanceof JSONArray) {
                            JSONArray jsonArray = (JSONArray) jaAns.get(i);
                            for (int k = 0; k < jsonArray.length(); k++) {
                                if (jsonArray.get(k).toString().equalsIgnoreCase(String.valueOf(text))) {
                                    isChecked = true;
                                } else {
                                    isChecked = false;
                                }
                            }
                        } else {*/
                        Object o = jaAns.getJSONObject(0).get(String.valueOf(yscale.get(i)));
                            String str = String.valueOf(o).replace("[","").replace("]","").
                                    replace("\"","");
                            if (str.equalsIgnoreCase(String.valueOf(text))) {
                                isChecked = true;
                            } else {
                                isChecked = false;
                            }
                       // }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    /*}*/
                } else {
                    isChecked = false;
                }
                Cell cell = new Cell(id, text, isChecked);
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<Cell>> getRandomCellList() {
        List<List<Cell>> list = new ArrayList<>();
        for (int i = 0; i < ROW_SIZE; i++) {
            List<Cell> cellList = new ArrayList<>();
            list.add(cellList);
            for (int j = 0; j < COLUMN_SIZE; j++) {
                String text = "cell " + j + " " + i;
                int random = new Random().nextInt();
                if (random % 2 == 0 || random % 5 == 0 || random == j) {
                    text = "large cell  " + j + " " + i + getRandomString() + ".";
                }

                // Create dummy id.
                String id = j + "-" + i;

                Cell cell = new Cell(id, text, false);
                cellList.add(cell);
            }
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<Cell>> getEmptyCellList() {
        List<List<Cell>> list = new ArrayList<>();
        for (int i = 0; i < ROW_SIZE; i++) {
            List<Cell> cellList = new ArrayList<>();
            list.add(cellList);
            for (int j = 0; j < COLUMN_SIZE; j++) {

                // Create dummy id.
                String id = j + "-" + i;

                Cell cell = new Cell(id, "", false);
                cellList.add(cell);
            }
        }

        return list;
    }

    private List<RowHeader> getEmptyRowHeaderList() {
        List<RowHeader> list = new ArrayList<>();
        for (int i = 0; i < ROW_SIZE; i++) {
            RowHeader header = new RowHeader(String.valueOf(i), "", false);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    public static List<List<Cell>> getRandomCellList(int startIndex) {
        List<List<Cell>> list = new ArrayList<>();
        for (int i = 0; i < ROW_SIZE; i++) {
            List<Cell> cellList = new ArrayList<>();
            list.add(cellList);
            for (int j = 0; j < COLUMN_SIZE; j++) {
                String text = "cell " + j + " " + (i + startIndex);
                int random = new Random().nextInt();
                if (random % 2 == 0 || random % 5 == 0 || random == j) {
                    text = "large cell  " + j + " " + (i + startIndex) + getRandomString() + ".";
                }

                String id = j + "-" + (i + startIndex);

                Cell cell = new Cell(id, text, false);
                cellList.add(cell);
            }
        }

        return list;
    }


    private static String getRandomString() {
        Random r = new Random();
        String str = " a ";
        for (int i = 0; i < r.nextInt(); i++) {
            str = str + " a ";
        }

        return str;
    }

    public List<List<Cell>> getCellList(JSONArray yscale, JSONArray xscale, JSONArray jaAns) {
        return getCellListForSortingTest(yscale, xscale, jaAns);
    }

    public List<RowHeader> getRowHeaderList(JSONArray rowheader) {
        return getSimpleRowHeaderList(rowheader);
    }

    public List<ColumnHeader> getColumnHeaderList(JSONArray columnheader) {
        return getRandomColumnHeaderList(columnheader);
    }


}
