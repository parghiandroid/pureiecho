package com.pure.pureiecho.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.pure.pureiecho.Activity.QuestionnaireScreen;
import com.pure.pureiecho.Model.Answer;
import com.pure.pureiecho.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RankOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<Answer> strOptions = new ArrayList<>();
    RankOrderSelection rankOrderSelection;
    int selectedPosition = -1;
    final ArrayList<String> stringsArray;

    public RankOrderAdapter(Context mContext, ArrayList<Answer> strOptions,
                            ArrayList<String> stringsArray) {
        this.mContext = mContext;
        this.strOptions = strOptions;
        this.stringsArray = stringsArray;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_rankorder_selection_option, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int Parent_position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            final Answer answer = strOptions.get(Parent_position);
            viewHolder.mtxt_rank_lable.setText(answer.getMtext());
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext,
                    android.R.layout.simple_spinner_dropdown_item, stringsArray);
            viewHolder.msp_rank_order_option.setAdapter(spinnerArrayAdapter);

            if(QuestionnaireScreen.rankOrder[Parent_position] != null) {
                viewHolder.msp_rank_order_option.setSelection(Integer.parseInt(QuestionnaireScreen.
                        rankOrder[Parent_position]) - 1);
            }
            viewHolder.msp_rank_order_option.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //Toast.makeText(MainActivity.this, getString(R.string.selected_item) + " " + personNames[position], Toast.LENGTH_SHORT).show();
                    QuestionnaireScreen.rankOrder[Parent_position] = stringsArray.get(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return strOptions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mtxt_rank_lable;
        private Spinner msp_rank_order_option;

        public ViewHolder(View itemView) {
            super(itemView);
            mtxt_rank_lable = itemView.findViewById(R.id.txt_rank_lable);
            msp_rank_order_option = itemView.findViewById(R.id.sp_rank_order_option);
        }
    }

    public interface RankOrderSelection {
        void SelectedRankOrderOption(int Position, String Value);
    }
}
