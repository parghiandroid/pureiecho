package com.pure.pureiecho.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pure.pureiecho.Activity.QuestionnaireScreen;
import com.pure.pureiecho.Global.Typefaces;
import com.pure.pureiecho.Model.Survey;
import com.pure.pureiecho.R;

import java.util.ArrayList;

public class SurveyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    ArrayList<Survey> surveys = new ArrayList<>();


    public SurveyAdapter(Context mContext, ArrayList<Survey> surveys) {
        this.mContext = mContext;
        this.surveys = surveys;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_survey, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            final Survey survey = surveys.get(position);
            viewHolder.mTxtSurvey.setText(++position + " . " + survey.getTitle());
            viewHolder.mLlSurvey.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, QuestionnaireScreen.class);
                    intent.putExtra("Survey_id", survey.getSurvey_id());
                    intent.putExtra("encode_survey_id", survey.getEncode_survey_id());
                    intent.putExtra("app_id", survey.getmApp_id());
                    intent.putExtra("app_secret", survey.getmApp_secret());
                    mContext.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return surveys.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtSurvey;
        private LinearLayout mLlSurvey;

        public ViewHolder(View itemView) {
            super(itemView);
            mTxtSurvey = itemView.findViewById(R.id.txtSurvey);
            mLlSurvey = itemView.findViewById(R.id.llSurvey);
            mTxtSurvey.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        }
    }
}
