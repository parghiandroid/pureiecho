package com.pure.pureiecho.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.pure.pureiecho.Model.Answer;
import com.pure.pureiecho.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CheckboxOptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<Answer> strOptions = new ArrayList<>();
    String strSelectOption = "";
    ArrayList<String> SelectedTextOption = new ArrayList<>();
    public static JSONArray jsonArray = new JSONArray();

    public CheckboxOptionAdapter(Context mContext, ArrayList<Answer> strOptions, String strSelectOption) {
        this.mContext = mContext;
        this.strOptions = strOptions;
        this.strSelectOption = strSelectOption;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_checkbox_option, parent,
                false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            final Answer answer = strOptions.get(position);
            viewHolder.mCkbOption.setText(answer.getMtext());
            if (!answer.getMimage().equalsIgnoreCase("")) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(answer.getMimage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                viewHolder.mimg_option.setVisibility(View.VISIBLE);
                Picasso.with(mContext).load(jsonObject.optString("200x200")).into(viewHolder.mimg_option);
            } else {
                viewHolder.mimg_option.setVisibility(View.GONE);
            }
            try {
                if (!strSelectOption.equals("")) {
                    JSONArray jsonArrayTextSelectOption = new JSONArray(strSelectOption);
                    if (jsonArrayTextSelectOption != null) {
                        boolean isSelect = false;
                        for (int i = 0; i < jsonArrayTextSelectOption.length(); i++) {
                            JSONObject jsonObject = (JSONObject) jsonArrayTextSelectOption.get(i);
                            if (!jsonObject.getString("text").equals("") && !jsonObject.getString("image").equals("")) {
                                if (jsonObject.optString("text").equals(answer.getMtext()) &&
                                        jsonObject.getString("image").equals(answer.getMimage())) {
                                    isSelect = true;
                                    JSONObject jsonAns = new JSONObject();
                                    try {
                                        jsonAns.put("text", answer.getMtext());
                                        jsonAns.put("image", answer.getMimage());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    jsonArray.put(jsonAns);
                                    SelectedTextOption.add(answer.getMtext());
                                    viewHolder.mCkbOption.setChecked(true);
                                } else {
                                    if (!isSelect) {
                                        viewHolder.mCkbOption.setChecked(false);
                                    }
                                }
                            } else if (!jsonObject.getString("text").equals("")) {
                                if (jsonObject.getString("text").equalsIgnoreCase(answer.getMtext())) {
                                    isSelect = true;
                                    JSONObject jsonAns = new JSONObject();
                                    try {
                                        jsonAns.put("text", answer.getMtext());
                                        jsonAns.put("image", answer.getMimage());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    jsonArray.put(jsonAns);
                                    SelectedTextOption.add(answer.getMtext());
                                    viewHolder.mCkbOption.setChecked(true);
                                } else {
                                    if (!isSelect) {
                                        viewHolder.mCkbOption.setChecked(false);
                                    }
                                }
                            } else if (!jsonObject.getString("image").equals("")) {
                                if (jsonObject.getString("image").equalsIgnoreCase(answer.getMimage())) {
                                    isSelect = true;
                                    JSONObject jsonAns = new JSONObject();
                                    try {
                                        jsonAns.put("text", answer.getMtext());
                                        jsonAns.put("image", answer.getMimage());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    jsonArray.put(jsonAns);
                                    viewHolder.mCkbOption.setChecked(true);
                                } else {
                                    if (!isSelect) {
                                        viewHolder.mCkbOption.setChecked(false);
                                    }
                                }
                            } else {
                                if (!isSelect) {
                                    viewHolder.mCkbOption.setChecked(false);
                                }
                            }
                        }
                    }
                } else {
                    viewHolder.mCkbOption.setChecked(false);
                }

            } catch (JSONException | NullPointerException e) {
                e.printStackTrace();
            }

            viewHolder.mCkbOption.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @SuppressLint("NewApi")
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        viewHolder.mCkbOption.setChecked(true);
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("text", answer.getMtext());
                            jsonObject.put("image", answer.getMimage());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        jsonArray.put(jsonObject);
                    } else {
                        viewHolder.mCkbOption.setChecked(false);
                        if (SelectedTextOption.size() > 0) {
                            if (SelectedTextOption.contains(strOptions.get(position))) {
                                SelectedTextOption.remove(strOptions.get(position));
                            }
                        }
                        for (int i = 0; i < strOptions.size(); i++) {
                            for (int j = 0; j < jsonArray.length(); j++) {
                                try {
                                    JSONObject jo = (JSONObject) jsonArray.get(j);
                                    if (!jo.optString("text").equals("")) {
                                        if (jo.optString("text").equals(answer.getMtext())) {
                                            jsonArray.remove(j);
                                        }
                                    } else if (!jo.optString("image").equals("")) {
                                        if (jo.optString("image").equals(answer.getMimage())) {
                                            jsonArray.remove(j);
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return strOptions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBox mCkbOption;
        private ImageView mimg_option;

        public ViewHolder(View itemView) {
            super(itemView);
            mCkbOption = itemView.findViewById(R.id.ckbOption);
            mimg_option = itemView.findViewById(R.id.img_option);
        }
    }
}
