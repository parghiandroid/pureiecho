package com.pure.pureiecho.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.pure.pureiecho.Model.Answer;
import com.pure.pureiecho.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RadioOptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<Answer> strOptions = new ArrayList<>();
    RadioOptionSelection radioOptionSelection;
    int selectedPosition = -1;
    String strSelectTextOption, strSelectImageOption;

    public RadioOptionAdapter(Context mContext, ArrayList<Answer> strOptions, String strSelectTextOption, String strSelectImageOption) {
        this.mContext = mContext;
        this.strOptions = strOptions;
        this.strSelectTextOption = strSelectTextOption;
        this.strSelectImageOption = strSelectImageOption;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_radio_option, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            final Answer answer = strOptions.get(position);
            viewHolder.mRbOption.setText(answer.getMtext());
            if (!answer.getMimage().equalsIgnoreCase("")) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(answer.getMimage());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                viewHolder.mimg_option.setVisibility(View.VISIBLE);
                Picasso.with(mContext).load(jsonObject.optString("200x200")).into(viewHolder.mimg_option);
            } else {
                viewHolder.mimg_option.setVisibility(View.GONE);
            }

            if (!strSelectTextOption.equalsIgnoreCase("") || !strSelectImageOption.equals("")) {
                if (!strSelectTextOption.equalsIgnoreCase("")) {
                    if (strSelectTextOption.equalsIgnoreCase(answer.getMtext())) {
                        viewHolder.mRbOption.setChecked(true);
                    } else {
                        viewHolder.mRbOption.setChecked(false);
                    }
                } else if (!strSelectImageOption.equalsIgnoreCase("")) {
                    if (strSelectImageOption.equalsIgnoreCase(answer.getMimage())) {
                        viewHolder.mRbOption.setChecked(true);
                    }
                } else {
                    viewHolder.mRbOption.setChecked(false);
                }
            } else {
                if (selectedPosition == position) {
                    viewHolder.mRbOption.setChecked(true);
                } else {
                    viewHolder.mRbOption.setChecked(false);
                }
            }

            viewHolder.mRbOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPosition = position;
                    notifyDataSetChanged();
                    radioOptionSelection = (RadioOptionSelection) mContext;
                    if (!answer.getMtext().equals("") && !answer.getMimage().equals("")) {
                        strSelectTextOption = answer.getMtext();
                        strSelectImageOption = answer.getMimage();
                        radioOptionSelection.SelectedRadioOption(position, answer.getMtext(), answer.getMimage());
                    } else if (!answer.getMtext().equals("")) {
                        strSelectTextOption = answer.getMtext();
                        radioOptionSelection.SelectedRadioOption(position, answer.getMtext(), "");
                    } else if (!answer.getMimage().equals("")) {
                        strSelectImageOption = answer.getMimage();
                        radioOptionSelection.SelectedRadioOption(position, "", answer.getMimage());
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return strOptions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RadioButton mRbOption;
        private ImageView mimg_option;

        public ViewHolder(View itemView) {
            super(itemView);
            mRbOption = itemView.findViewById(R.id.rbOption);
            mimg_option = itemView.findViewById(R.id.img_option);
        }
    }

    public interface RadioOptionSelection {
        void SelectedRadioOption(int Position, String TextValue, String ImageValue);
    }
}
