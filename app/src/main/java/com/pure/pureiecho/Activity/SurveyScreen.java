package com.pure.pureiecho.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pure.pureiecho.Adapter.SurveyAdapter;
import com.pure.pureiecho.Global.Global;
import com.pure.pureiecho.Global.SharedPreference;
import com.pure.pureiecho.Global.StaticUtility;
import com.pure.pureiecho.Global.Typefaces;
import com.pure.pureiecho.Model.Survey;
import com.pure.pureiecho.R;
import com.pure.pureiecho.Service.API;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;

public class SurveyScreen extends AppCompatActivity implements View.OnClickListener, API.JSONResponse {
    private Context mContext = SurveyScreen.this;
    private String TAG = LoginScreen.class.getSimpleName();
    //Event Bus (Internet)
    public EventBus eventBus = EventBus.getDefault();
    //Widget
    private RecyclerView mRvSurvey;

    //Toolbar
    private TextView mTxtTitle, mtxt_not_found;
    private ImageView mImgBack, mImgLogout;

    //Loader
    private RelativeLayout mRlLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_survey_screen);
        eventBus.register(this);
        initializeWidget();
        setTypeFaces();
        setWidgetOperations();
        if(Global.isNetworkAvailable(mContext)){
            GetSurvey();
        }else {
            Global.AlertDailog(mContext, "Not connected to Internet");
        }
    }

    //region initializeWidget
    private void initializeWidget() {
        //Toolbar
        mtxt_not_found = findViewById(R.id.txt_not_found);
        mTxtTitle = findViewById(R.id.txtTitle);
        mImgBack = findViewById(R.id.imgBack);
        mImgLogout = findViewById(R.id.imgLogout);
        //widget
        mRvSurvey = findViewById(R.id.rvSurvey);
        //Loader
        mRlLoader = findViewById(R.id.rlLoader);
    }//endregion

    //region setTypeFaces
    private void setTypeFaces() {
        mTxtTitle.setTypeface(Typefaces.Snippet(mContext));
        mtxt_not_found.setTypeface(Typefaces.Snippet(mContext));

    }//endregion

    //region setWidgetOperations
    private void setWidgetOperations() {
        mImgLogout.setOnClickListener(this);
        mImgBack.setOnClickListener(this);
    }//endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        Global.AlertDailog(mContext, event);
    }//endregion

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgLogout:
                new AlertDialog.Builder(mContext)
                        .setMessage(R.string.logout_message)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                SharedPreference.ClearPreference(mContext, StaticUtility.LOGIN_PREFERENCE);
                                Intent intent = new Intent(mContext, LoginScreen.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                break;

            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

    //region Get Survey List
    private void GetSurvey() {
        mRlLoader.setVisibility(View.VISIBLE);
        String[] key = {};
        String[] val = {};
        API.APICalling(mContext, null, TAG, "surveys", StaticUtility.GETSURVEYS, key, val, Global.headers());
    }//endregion

    @Override
    public void JsonAPIResponse(String strAPIType, JSONObject response) {
        String strCode = response.optString("code");
        String strStatus = response.optString("status");
        String strMessage = response.optString("message");
        if (strCode.equals("200")) {
            if (strAPIType.equalsIgnoreCase("surveys")) {
                try {
                    mRlLoader.setVisibility(View.GONE);
                    JSONArray jsonArrayPayload = response.optJSONArray("payload");
                    if (jsonArrayPayload.length() > 0) {
                        ArrayList<Survey> surveys = new ArrayList<>();
                        for (int i = 0; i < jsonArrayPayload.length(); i++) {
                            JSONObject jsonObjectPayload = (JSONObject) jsonArrayPayload.get(i);
                            String strSurveyId = jsonObjectPayload.optString("survey_id");
                            String strEncodedSurveyId = jsonObjectPayload.optString("encode_survey_id");
                            String strTitle = jsonObjectPayload.optString("title");
                            String strApp_id = jsonObjectPayload.optString("encode_app_id");
                            String strApp_secret = jsonObjectPayload.optString("encode_app_secret");
                            surveys.add(new Survey(strSurveyId, strEncodedSurveyId, strTitle, strApp_id, strApp_secret));
                        }
                        SurveyAdapter surveyAdapter = new SurveyAdapter(mContext, surveys);
                        mRvSurvey.setLayoutManager(new LinearLayoutManager(mContext,
                                LinearLayoutManager.VERTICAL, false));
                        mRvSurvey.setAdapter(surveyAdapter);
                    }else {
                        mRvSurvey.setVisibility(View.GONE);
                        mtxt_not_found.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            mRlLoader.setVisibility(View.GONE);
            Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
        }
    }
}
