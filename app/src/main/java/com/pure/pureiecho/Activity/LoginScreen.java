package com.pure.pureiecho.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pure.pureiecho.Global.BodyParam;
import com.pure.pureiecho.Global.Global;
import com.pure.pureiecho.Global.SharedPreference;
import com.pure.pureiecho.Global.StaticUtility;
import com.pure.pureiecho.Global.Typefaces;
import com.pure.pureiecho.R;
import com.pure.pureiecho.Service.API;

import org.json.JSONObject;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener,
        TextWatcher, API.JSONResponse {

    //Context
    Context mContext = LoginScreen.this;
    private String TAG = LoginScreen.class.getSimpleName();
    //Event Bus (Internet)
    public EventBus eventBus = EventBus.getDefault();
    //Widget
    private TextView mTxtTitle;
    private EditText mEdtUserName, mEdtUserEmail, mEdtPassword, mEdtMobileNo;
    private CardView mCvSubmit;
    private TextView mTxtSubmit;
    //Loader
    private RelativeLayout mRlLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_login_screen);
        eventBus.register(this);


        initializeWidget();
        setTypeFaces();
        setWidgetOperations();
    }

    //region initializeWidget
    private void initializeWidget() {
        mTxtTitle = findViewById(R.id.txtTitle);
        mEdtUserName = findViewById(R.id.edtUserName);
        mEdtUserEmail = findViewById(R.id.edtUserEmail);
        mEdtPassword = findViewById(R.id.edtPassword);
        mEdtMobileNo = findViewById(R.id.edtMobileNo);
        mCvSubmit = findViewById(R.id.cvSubmit);
        mTxtSubmit = findViewById(R.id.txtSubmit);
        //Loader
        mRlLoader = findViewById(R.id.rlLoader);
    }//endregion

    //region setTypeFaces
    private void setTypeFaces() {
        mTxtTitle.setTypeface(Typefaces.Snippet(mContext));
        mEdtUserName.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mEdtUserEmail.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mEdtPassword.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mTxtSubmit.setTypeface(Typefaces.Ubuntu_Regular(mContext));
    }//endregion

    //region setWidgetOperations
    private void setWidgetOperations() {
        mCvSubmit.setOnClickListener(this);
        mEdtUserName.addTextChangedListener(this);
        mEdtUserEmail.addTextChangedListener(this);
        mEdtPassword.addTextChangedListener(this);
        mEdtMobileNo.addTextChangedListener(this);
    }//endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        Global.AlertDailog(mContext, event);
    }//endregion

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cvSubmit:
                if(Global.isNetworkAvailable(mContext)) {
                    if (Validation()) {
                        mRlLoader.setVisibility(View.VISIBLE);
                        String[] key = {BodyParam.pNAME, BodyParam.pEMAIL, BodyParam.pPHONE};
                        String[] value = {mEdtUserName.getText().toString(),
                                mEdtUserEmail.getText().toString(),
                                mEdtMobileNo.getText().toString()};
                        API.APICalling(mContext, null, TAG, "login",
                                StaticUtility.SAVECLIENT, key, value, Global.headers());
                    }
                }else {
                    Global.AlertDailog(mContext, "Not connected to Internet");
                }
                break;
        }
    }

    //region Validation
    @SuppressLint("NewApi")
    public boolean Validation() {
        if (!TextUtils.isEmpty(mEdtUserName.getText().toString())) {
            mEdtUserName.setError(null);
            if (!TextUtils.isEmpty(mEdtUserEmail.getText().toString())) {
                mEdtUserEmail.setError(null);
                if (Global.isValidEmail(mEdtUserEmail.getText().toString())) {
                    mEdtUserEmail.setError(null);
                    if (!TextUtils.isEmpty(mEdtMobileNo.getText().toString())) {
                        mEdtMobileNo.setError(null);
                        if (mEdtMobileNo.getText().length() >= 10) {
                            mEdtMobileNo.setError(null);
                            mEdtMobileNo.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_images)));
                            return true;
                       /* if (!TextUtils.isEmpty(mEdtPassword.getText().toString())) {
                            mEdtPassword.setError(null);

                        } else {
                            mEdtPassword.setError("Enter Password..!");
                            mEdtPassword.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_red)));
                        }*/
                        } else {
                            mEdtMobileNo.setError("Enter Valid Mobile No..!");
                            mEdtMobileNo.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_red)));
                        }
                    } else {
                        mEdtMobileNo.setError("Enter Mobile No..!");
                        mEdtMobileNo.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_red)));
                    }
                } else {
                    mEdtUserEmail.setError("Enter Valid Email Id..!");
                    mEdtUserEmail.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_red)));
                }
            } else {
                mEdtUserEmail.setError("Enter Email Id..!");
                mEdtUserEmail.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_red)));
            }
        } else {
            mEdtUserName.setError("Enter User Name..!");
            mEdtUserName.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_red)));
        }
        return false;
    }//endregion

    //region TextChanged
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @SuppressLint("NewApi")
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() > 0) {
            if (mEdtUserName.getText().hashCode() == s.hashCode()) {
                mEdtUserName.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_images)));
            } else if (mEdtUserEmail.getText().hashCode() == s.hashCode()) {
                mEdtUserEmail.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_images)));
            } else if (mEdtPassword.getText().hashCode() == s.hashCode()) {
                mEdtPassword.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_images)));
            } else if (mEdtMobileNo.getText().hashCode() == s.hashCode()) {
                mEdtMobileNo.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_images)));
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }//endregion

    @Override
    public void JsonAPIResponse(String strAPIType, JSONObject response) {
        String strCode = response.optString("code");
        String strStatus = response.optString("status");
        String strMessage = response.optString("message");
        if (strCode.equals("200")) {
            if (strAPIType.equalsIgnoreCase("login")) {
                mRlLoader.setVisibility(View.GONE);
                JSONObject jsonObjectPayload = response.optJSONObject("payload");
                String strClientId = jsonObjectPayload.optString("client_id");
                String strName = jsonObjectPayload.optString("name");
                String strEmail = jsonObjectPayload.optString("email");
                String strPhone = jsonObjectPayload.optString("phone");
                SharedPreference.CreatePreference(mContext, StaticUtility.LOGIN_PREFERENCE);
                SharedPreference.SavePreference(StaticUtility.sCLIENT_ID, strClientId);
                SharedPreference.SavePreference(StaticUtility.sUSERNAME, strName);
                SharedPreference.SavePreference(StaticUtility.sUSEREMAIL, strEmail);
                SharedPreference.SavePreference(StaticUtility.sUSEREPHONE, strPhone);
                Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(LoginScreen.this, SurveyScreen.class);
                startActivity(i);
                finish();
            }
        } else {
            mRlLoader.setVisibility(View.GONE);
            Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
        }
    }
}
