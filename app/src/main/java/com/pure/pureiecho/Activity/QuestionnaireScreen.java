package com.pure.pureiecho.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.androidbuts.multispinnerfilter.MultiSpinner;
import com.androidbuts.multispinnerfilter.MultiSpinnerListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.evrencoskun.tableview.TableView;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.hsalf.smilerating.BaseRating;
import com.hsalf.smilerating.SmileRating;
import com.pure.pureiecho.Adapter.CheckboxOptionAdapter;
import com.pure.pureiecho.Adapter.ImageOptionAdapter;
import com.pure.pureiecho.Adapter.RadioOptionAdapter;
import com.pure.pureiecho.Adapter.RankOrderAdapter;
import com.pure.pureiecho.Global.BodyParam;
import com.pure.pureiecho.Global.FilePath;
import com.pure.pureiecho.Global.Global;
import com.pure.pureiecho.Global.SharedPreference;
import com.pure.pureiecho.Global.StaticUtility;
import com.pure.pureiecho.Global.Typefaces;
import com.pure.pureiecho.Model.Answer;
import com.pure.pureiecho.Model.Question;
import com.pure.pureiecho.R;
import com.pure.pureiecho.Service.API;
import com.pure.pureiecho.Service.AppController;
import com.pure.pureiecho.tableview.TableViewAdapter;
import com.pure.pureiecho.tableview.TableViewListener;
import com.pure.pureiecho.tableview.TableViewModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;

public class QuestionnaireScreen extends AppCompatActivity implements View.OnClickListener,
        ImageOptionAdapter.AnswerSelection, RadioOptionAdapter.RadioOptionSelection, API.JSONResponse {
    //Context
    private Context mContext = QuestionnaireScreen.this;
    private String TAG = QuestionnaireScreen.class.getSimpleName();

    //Event Bus (Internet)
    public EventBus eventBus = EventBus.getDefault();

    //widget
    private TextView mTxtQuesTitle, mTxtAnsCount, mTxtQuesCount, mTxtQues;
    private ImageView mimg_question;
    private ProgressBar mPbQuestion;
    private LinearLayout mLlAnswer, mLlBottom;
    private LinearLayout mLlBack, mLlNext;
    private TextView mTxtBack, mTxtNext;
    int intTotalQues = 0;
    int intQuesCount = 1;
    int intQus = 0;
    public static String[] multipoint;
    public static String[][] multiselect;
    public static String[] rankOrder;

    //Toolbar
    private TextView mTxtTitle;
    private ImageView mImgBack, mImgLogout;

    ArrayList<Question> questions;
    String[] answers;

    private EditText mEdtText, mEdtMutiLine;
    private TextView mtxt_date_time, mtxt_got_it;
    private RadioGroup mRadiogroup;

    private static ArrayList<String> arrayListCheckbox;
    private static JSONArray jsonArrayCheckbox = new JSONArray();

    private String strOptionImageValue = "", strOptionRadioTextValue = "", strOptionRadioImageValue = "", strOptionSpinervalue = "",
            strOptionMultiSpinnervalue = "", strSingleCheckboxValue = "off",
            strCaptchaStatusValue = "", strRatingValue = "", strSmileyRatingValue = "", strFileUploadValue = "", strFileDisplayName = "";

    //Loader
    private RelativeLayout mRlLoader, mshowcase;

    private String strSurveyID = "", strEncodeSurveyID = "", mApp_id, mApp_secret;

    //multi spinner
    private MultiSpinner msimpleMultiSpinner;

    private int mYear, mMonth, mDay, mHour, mMinute;

    //Tableview
    private TableView mTableView;
    private TableViewModel mTableViewModel;
    private TableViewAdapter mTableViewAdapter;

    private static final String URL_VERIFY_ON_SERVER = "https://www.google.com/recaptcha/api/siteverify";
    /*private static final String URL_VERIFY_ON_SERVER = "http://192.168.0.113/pureiecho/purerobo/test.php";*/
    private static int PICKFILE_REQUEST_CODE = 42;

    TextView mTxtFileUpload;

    private JSONArray jaMultiSelection = new JSONArray();

    String[] list;
    boolean[] checkedList;
    ArrayList<Integer> integers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_questionnaire_screen);
        eventBus.register(this);

        strSurveyID = getIntent().getStringExtra("Survey_id");
        strEncodeSurveyID = getIntent().getStringExtra("encode_survey_id");
        mApp_id = getIntent().getStringExtra("app_id");
        mApp_secret = getIntent().getStringExtra("app_secret");

        initializeWidget();
        setTypeFaces();
        setWidgetOperations();
        mPbQuestion.setProgress(intQuesCount);
        if (Global.isNetworkAvailable(mContext)) {
            GetQuestionary();
        } else {
            Global.AlertDailog(mContext, "Not connected to Internet");
        }
    }

    //region initializeWidget
    private void initializeWidget() {
        mTxtQuesTitle = findViewById(R.id.txtQuesTitle);
        mTxtAnsCount = findViewById(R.id.txtAnsCount);
        mTxtQuesCount = findViewById(R.id.txtQuesCount);
        mPbQuestion = findViewById(R.id.pbQuestion);
        mTxtQues = findViewById(R.id.txtQues);
        mLlAnswer = findViewById(R.id.llAnswer);
        mLlBottom = findViewById(R.id.llBottom);
        mLlBack = findViewById(R.id.llBack);
        mTxtBack = findViewById(R.id.txtBack);
        mLlNext = findViewById(R.id.llNext);
        mTxtNext = findViewById(R.id.txtNext);

        //Imageview
        mimg_question = findViewById(R.id.img_question);

        //Toolbar
        mTxtTitle = findViewById(R.id.txtTitle);
        mImgBack = findViewById(R.id.imgBack);
        mImgLogout = findViewById(R.id.imgLogout);

        //Loader
        mRlLoader = findViewById(R.id.rlLoader);

        //Multi spinner
        msimpleMultiSpinner = findViewById(R.id.simpleMultiSpinner);

        //Tableview
        mTableView = findViewById(R.id.tableview);

        //showcase
        mshowcase = findViewById(R.id.showcase);
        mtxt_got_it = findViewById(R.id.txt_got_it);

    }//endregion

    //region setTypeFaces
    private void setTypeFaces() {
        mTxtQuesTitle.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mTxtAnsCount.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mTxtQuesCount.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mTxtBack.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mTxtNext.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mTxtTitle.setTypeface(Typefaces.Snippet(mContext));
    }//endregion

    //region setWidgetOperations
    private void setWidgetOperations() {
        mLlBack.setOnClickListener(this);
        mLlNext.setOnClickListener(this);
        mImgLogout.setOnClickListener(this);
        mImgBack.setOnClickListener(this);
        mtxt_got_it.setOnClickListener(this);

    }//endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        Global.AlertDailog(mContext, event);
    }//endregion

    //region onClick
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llNext:
                if (mTxtNext.getText().toString().equalsIgnoreCase("Next")) {
                    if (Validate()) {
                        strFileDisplayName = "";
                        if (intQuesCount < intTotalQues) {
                            intQuesCount++;
                            intQus++;
                            NextQues(intQuesCount);
                            mLlBack.setVisibility(View.VISIBLE);
                            /*if (intQuesCount == intTotalQues) {
                                mTxtNext.setText(getString(R.string.submit));
                            } else {
                                mTxtNext.setText(getString(R.string.next));
                            }*/
                        }
                    }
                } else {
                    if (Validate()) {
                        if (Global.isNetworkAvailable(mContext)) {
                            SaveSurvey();
                        } else {
                            Global.AlertDailog(mContext, "Not connected to Internet");
                        }
                    }
                }
                break;

            case R.id.llBack:
                if (intQuesCount > 1) {
                    if (addanswer()) {
                        intQuesCount--;
                        intQus--;
                        strFileDisplayName = "";
                        if (intQuesCount == 1) {
                            mLlBack.setVisibility(View.GONE);
                        }
                        PreviousQues(intQuesCount);
                        if (intQuesCount == intTotalQues) {
                            mTxtNext.setText(getString(R.string.submit));
                        } else {
                            mTxtNext.setText(getString(R.string.next));
                        }
                    }
                }
                break;

            case R.id.imgLogout:
                new AlertDialog.Builder(mContext)
                        .setMessage(R.string.logout_message)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                SharedPreference.ClearPreference(mContext, StaticUtility.LOGIN_PREFERENCE);
                                Intent intent = new Intent(mContext, LoginScreen.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                        Intent.FLAG_ACTIVITY_NEW_TASK |
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                break;
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.txt_got_it:
                mshowcase.setVisibility(View.GONE);
                break;
        }
    }//endregion

    //region NextQues
    private void NextQues(int Count) {
        mPbQuestion.setProgress(Count);
        mTxtAnsCount.setText(String.valueOf(intQuesCount) + " / ");
        mLlAnswer.removeAllViews();
        setQuestions();
    }
    //endregion

    //region PreviousQues
    private void PreviousQues(int Count) {
        mPbQuestion.setProgress(Count);
        mTxtAnsCount.setText(String.valueOf(intQuesCount) + " / ");
        mLlAnswer.removeAllViews();
        setQuestions();
    }
    //endregion

    //region Get Questionary List
    private void GetQuestionary() {
        mRlLoader.setVisibility(View.VISIBLE);
        String[] key = {BodyParam.pSERVEY_ID, BodyParam.pApp_Id, BodyParam.pApp_Secret};
        String[] val = {strEncodeSurveyID, mApp_id, mApp_secret};
        API.APICalling(mContext, null, TAG, "questionary", StaticUtility.GETQUESTIONS, key, val, Global.headers());
    }
    //endregion

    @Override
    public void JsonAPIResponse(String strAPIType, JSONObject response) {
        String strCode = response.optString("code");
        String strStatus = response.optString("status");
        String strMessage = response.optString("message");
        if (strCode.equals("200")) {
            if (strAPIType.equalsIgnoreCase("questionary")) {
                try {
                    mRlLoader.setVisibility(View.GONE);
                    JSONObject jsonArrayPayload = response.optJSONObject("payload");
                    JSONArray jsonArrayquestion = jsonArrayPayload.optJSONArray("questiondata");
                    if (jsonArrayquestion != null && jsonArrayquestion.length() > 0) {
                        questions = new ArrayList<>();
                        for (int i = 0; i < jsonArrayquestion.length(); i++) {
                            JSONObject jsonObjectQuestion = (JSONObject) jsonArrayquestion.get(i);
                            String strQuestionId = jsonObjectQuestion.optString("question_id");
                            String strQuestion = jsonObjectQuestion.optString("question");
                            Object objimage = jsonObjectQuestion.opt("question_image");
                            String strQuestion_image = "";
                            String strQuestion_image_ans = "";
                            if (!objimage.toString().equals("")) {
                                strQuestion_image = jsonObjectQuestion.optJSONObject("question_image")
                                        .optString("200x200");
                                strQuestion_image_ans = jsonObjectQuestion.optJSONObject("question_image").toString();
                            }
                            String strAnsType = jsonObjectQuestion.optString("ans_type");
                            String strSortOrder = jsonObjectQuestion.optString("sort_order");
                            String strIsMandatory = jsonObjectQuestion.optString("is_mandatory");
                            String strAnsDisplayType = jsonObjectQuestion.optString("ans_display_type");
                            Object objectAnsValue = jsonObjectQuestion.get("ans_values");
                            if (!objectAnsValue.equals("") && !strAnsDisplayType.equals("Date/Time")) {
                                JSONArray jsonArrayAnsValue = jsonObjectQuestion.optJSONArray("ans_values");
                                ArrayList<Answer> strAnsValue = new ArrayList<>();
                                for (int j = 0; j < jsonArrayAnsValue.length(); j++) {
                                    JSONObject objans;
                                    if (strAnsDisplayType.equalsIgnoreCase("Radio") ||
                                            strAnsDisplayType.equalsIgnoreCase("Checkbox")) {
                                        objans = jsonArrayAnsValue.optJSONObject(j);
                                        Object objText = objans.get("text");
                                        Object objImage = objans.get("image");
                                        if (!objText.equals("") && !objImage.toString().equals("[]") && !objImage.toString().equals("")) {
                                            strAnsValue.add(new Answer(String.valueOf(objans.optString("text")),
                                                    objans.optJSONObject("image").toString()));
                                        } else if (!objImage.toString().equals("[]") && !objImage.toString().equals("")) {
                                            strAnsValue.add(new Answer("", objans.optJSONObject("image").toString()));
                                        } else if (!objText.equals("")) {
                                            strAnsValue.add(new Answer(objans.optString("text"), ""));
                                        }
                                    } else if (strAnsDisplayType.equalsIgnoreCase("Dropdown") ||
                                            strAnsDisplayType.equalsIgnoreCase("Multi Select") ||
                                            strAnsDisplayType.equalsIgnoreCase("Rank Order")) {
                                        strAnsValue.add(new Answer(jsonArrayAnsValue.get(j).toString()));
                                    } else if (strAnsDisplayType.equalsIgnoreCase("Multi-Point Scales") ||
                                            strAnsDisplayType.equalsIgnoreCase("Multi-Select")) {
                                        objans = jsonArrayAnsValue.optJSONObject(j);
                                        strAnsValue.add(new Answer(objans.getJSONArray("xscale"),
                                                objans.getJSONArray("yscale")));
                                    } else if (strAnsDisplayType.equalsIgnoreCase("File Upload")) {
                                        objans = jsonArrayAnsValue.optJSONObject(j);
                                        strAnsValue.add(new Answer(objans));
                                    } else if (strAnsDisplayType.equalsIgnoreCase("Captcha")) {
                                        objans = jsonArrayAnsValue.optJSONObject(j);
                                        strAnsValue.add(new Answer(objans));
                                    } else if (strAnsDisplayType.equalsIgnoreCase("Terms & Condition")) {
                                        objans = jsonArrayAnsValue.optJSONObject(j);
                                        strAnsValue.add(new Answer(objans));
                                    } else if (strAnsDisplayType.equalsIgnoreCase("Rating")) {
                                        objans = jsonArrayAnsValue.optJSONObject(j);
                                        strAnsValue.add(new Answer(objans));
                                    } else if (strAnsDisplayType.equalsIgnoreCase("Smiley")) {
                                        objans = jsonArrayAnsValue.optJSONObject(j);
                                        strAnsValue.add(new Answer(objans));
                                    }
                                }
                                questions.add(new Question(strQuestionId, strQuestion, strQuestion_image, strQuestion_image_ans,
                                        strIsMandatory, strAnsDisplayType, strAnsValue));
                            } else {
                                questions.add(new Question(strQuestionId, strQuestion, strQuestion_image,
                                        strQuestion_image_ans, strIsMandatory, strAnsDisplayType, objectAnsValue.toString()));
                            }
                        }
                        answers = new String[questions.size()];
                        intTotalQues = questions.size();
                        mPbQuestion.setMax(intTotalQues);
                        mTxtQuesCount.setText(String.valueOf(intTotalQues));
                        mTxtAnsCount.setText(String.valueOf(intQuesCount) + " / ");
                        setQuestions();
                    } else {
                        Toast.makeText(mContext, "Survey not found...!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (strAPIType.equalsIgnoreCase("savesurvey")) {
                Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, ThankYouScreen.class);
                startActivity(intent);
                finish();
            }
        } else {
            mRlLoader.setVisibility(View.GONE);
            Toast.makeText(mContext, strMessage, Toast.LENGTH_SHORT).show();
        }
    }

    //region Save Survey List
    private void SaveSurvey() {
        mRlLoader.setVisibility(View.VISIBLE);
        String strClientId = SharedPreference.GetPreference(mContext, StaticUtility.LOGIN_PREFERENCE, StaticUtility.sCLIENT_ID);
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray jsonArrayQuestions = new JSONArray();
            JSONObject joUserinfo = new JSONObject();
            for (int i = 0; i < questions.size(); i++) {
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("question", questions.get(i).getQuestion());
                    if(!questions.get(i).getStrQuestion_image_ans().equalsIgnoreCase("")) {
                        obj.put("question_image", new JSONObject(questions.get(i).getStrQuestion_image_ans()));
                    }else {
                        obj.put("question_image",questions.get(i).getStrQuestion_image_ans());
                    }
                    obj.put("question_type", questions.get(i).getAns_display_type());
                    if (questions.get(i).getAns_display_type().equalsIgnoreCase("Rank Order")) {
                        if (!answers[i].equals("")) {
                            JSONArray jsonArray = new JSONArray(answers[i]);
                            obj.put("answer", jsonArray);
                        } else {
                            obj.put("answer", answers[i]);
                        }
                    } else if (questions.get(i).getAns_display_type().equalsIgnoreCase("checkbox")) {
                        if (!answers[i].equals("")) {
                            JSONArray jsonArray = new JSONArray(answers[i]);
                            JSONObject object = null;
                            for (int j = 0; j < jsonArray.length(); j++) {
                                object = new JSONObject();
                                object.put(String.valueOf(j + 1), jsonArray.optJSONObject(j));
                            }
                            obj.put("answer", object);
                        } else {
                            obj.put("answer", answers[i]);
                        }
                    } else if ("Multi-Select".equalsIgnoreCase(questions.get(i).getAns_display_type())) {
                        if (!answers[i].equals("")) {
                            JSONArray jsonArray = new JSONArray(answers[i]);
                            obj.put("answer", jsonArray.optJSONObject(0));
                        } else {
                            obj.put("answer", answers[i]);
                        }
                    } else if (questions.get(i).getAns_display_type().equalsIgnoreCase("Multi Select")) {
                        if (!answers[i].equals("")) {
                            JSONArray jsonArray = new JSONArray(answers[i]);
                            obj.put("answer", jsonArray);
                        } else {
                            obj.put("answer", answers[i]);
                        }
                    } else if (questions.get(i).getAns_display_type().equalsIgnoreCase("Multi-Point Scales")) {
                        if (!answers[i].equals("")) {
                            JSONArray jsonArray = new JSONArray(answers[i]);
                            obj.put("answer", jsonArray.optJSONObject(0));
                        } else {
                            obj.put("answer", answers[i]);
                        }
                    } else if (questions.get(i).getAns_display_type().equalsIgnoreCase("radio")) {
                        obj.put("question_id", questions.get(i).getQuestion_id());
                        if (!answers[i].equals("")) {
                            JSONObject jsonObject1 = new JSONObject(answers[i]);
                            obj.put("answer", jsonObject1);
                        } else {
                            obj.put("answer", answers[i]);
                        }
                    } else if (questions.get(i).getAns_display_type().equalsIgnoreCase("File Upload")) {
                        if (answers[i] != null && !answers[i].equalsIgnoreCase("")) {
                            JSONObject jsonObject1 = new JSONObject(answers[i]);
                            obj.put("answer", jsonObject1);
                            if (questions.get(i).getOptions().get(0).getMobject().optString("select_format").
                                    equalsIgnoreCase("pdf")) {
                                obj.put("file_type", "pdf");
                            } else if (questions.get(i).getOptions().get(0).getMobject().optString("select_format").
                                    equalsIgnoreCase("gif")) {
                                obj.put("file_type", "gif");
                            } else if (questions.get(i).getOptions().get(0).getMobject().optString("select_format").
                                    equalsIgnoreCase("jpg")) {
                                obj.put("file_type", "jpg");
                            } else if (questions.get(i).getOptions().get(0).getMobject().optString("select_format").
                                    equalsIgnoreCase("png")) {
                                obj.put("file_type", "png");
                            }
                        } else {
                            obj.put("answer", "");
                        }
                    } else {
                        obj.put("answer", answers[i]);
                    }
                    obj.put("is_required", questions.get(i).getIs_mandatory());
                    /*obj.put("answer", answers1.get(i));*/
                    jsonArrayQuestions.put(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (SharedPreference.GetPreference(mContext, StaticUtility.LOGIN_PREFERENCE, StaticUtility.sUSERNAME) != null) {
                String strName = SharedPreference.GetPreference(mContext, StaticUtility.LOGIN_PREFERENCE, StaticUtility.sUSERNAME);
                joUserinfo.put("name", strName);
            }
            if (SharedPreference.GetPreference(mContext, StaticUtility.LOGIN_PREFERENCE, StaticUtility.sUSEREMAIL) != null) {
                String strEmail = SharedPreference.GetPreference(mContext, StaticUtility.LOGIN_PREFERENCE, StaticUtility.sUSEREMAIL);
                joUserinfo.put("email", strEmail);
            }
            if (SharedPreference.GetPreference(mContext, StaticUtility.LOGIN_PREFERENCE, StaticUtility.sUSEREPHONE) != null) {
                String strPhone = SharedPreference.GetPreference(mContext, StaticUtility.LOGIN_PREFERENCE, StaticUtility.sUSEREPHONE);
                joUserinfo.put("phone", strPhone);
            }
            jsonObject.put(BodyParam.pSERVEY_ID, strEncodeSurveyID);
            jsonObject.put(BodyParam.pUSERINFO, joUserinfo);
            jsonObject.put(BodyParam.pQUESTIONS, jsonArrayQuestions);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("dataparam", jsonObject.toString());
        API.APICallingJson(mContext, null, TAG, "savesurvey",
                StaticUtility.SUBMITSURVEY, jsonObject, Global.headers());
    }//endregion

    //region set Questions
    private void setQuestions() {
        //setmultipointscale();
        if (intQuesCount == intTotalQues) {
            mTxtNext.setText(getString(R.string.submit));
        } else {
            mTxtNext.setText(getString(R.string.next));
        }
        mTableView.setVisibility(View.GONE);
        mLlAnswer.setVisibility(View.VISIBLE);
        msimpleMultiSpinner.setVisibility(View.GONE);
        if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
            String que = intQuesCount + " . " + questions.get(intQus).getQuestion() + " <font color=#FF0000>*</font>";
            mTxtQues.setText(Html.fromHtml(que));
        } else {
            mTxtQues.setText(intQuesCount + " . " + questions.get(intQus).getQuestion());
        }
        if (!questions.get(intQus).getMquestion_image().equalsIgnoreCase("")) {
            mimg_question.setVisibility(View.VISIBLE);
            Picasso.with(mContext).load(questions.get(intQus).getMquestion_image()).into(mimg_question);
        } else {
            mimg_question.setVisibility(View.GONE);
        }
        if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Text")) {
            createEditText();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Textarea")) {
            createMultiLineEditText();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Radio")) {
            createRadio();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Checkbox")) {
            createCheckBox();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("image")) {
            CreateImageViewRecyclerView();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Dropdown")) {
            createDropdown();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Multi Select")) {
            createMultiSpinner1();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Date/Time")) {
            createTextView(questions.get(intQus).getAns_values());
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Multi-Point Scales") ||
                questions.get(intQus).getAns_display_type().equalsIgnoreCase("Multi-Select")) {
            setmultipointscale();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Rank Order")) {
            createsetRankOrder();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Captcha")) {
            createCaptcha(questions.get(intQus).getQuestion());
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Terms & Condition")) {
            createSingleCheckBox();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Rating")) {
            createRatingbar();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Smiley")) {
            createSmileyRating();
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("File Upload")) {
            createFileUpload(questions.get(intQus).getQuestion());
        }
    }//endregion

    //region For create spinner for rank Order
    public void createsetRankOrder() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                110);
        layoutParams.setMargins(50, 0, 50, 10);
        RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setLayoutParams(layoutParams);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false));
        ArrayList<Answer> strOptions = questions.get(intQus).getOptions();
        final ArrayList<String> stringsArray = new ArrayList<>();
        for (int i = 0; i < strOptions.size(); i++) {
            stringsArray.add(String.valueOf(i + 1));
        }
        if (rankOrder == null) {
            rankOrder = new String[stringsArray.size()];
        }
        RankOrderAdapter rankOrderAdapter = null;
        if (questions.get(intQus).getOptions() != null && questions.get(intQus).getOptions().size() > 0) {
            if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
                /*strOptionRadioValue = answers[intQus];*/
                rankOrderAdapter = new RankOrderAdapter(mContext, questions.get(intQus).getOptions(),
                        stringsArray);
            } else {
                /*strOptionRadioValue = "";*/
                rankOrderAdapter = new RankOrderAdapter(mContext, questions.get(intQus).getOptions(),
                        stringsArray);
            }
            recyclerView.setAdapter(rankOrderAdapter);
        }
        mLlAnswer.addView(recyclerView);
    }
    //endregion

    //region For create setmultipointscale
    public void setmultipointscale() {
        mshowcase.setVisibility(View.VISIBLE);
        mTableView.setVisibility(View.VISIBLE);
        mLlAnswer.setVisibility(View.GONE);
        msimpleMultiSpinner.setVisibility(View.GONE);

        // Create TableView View model class  to group view models of TableView
        mTableViewModel = new TableViewModel(mContext);

        // Create TableView Adapter
        if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Multi-Point Scales")) {
            if (multipoint == null) {
                multipoint = new String[questions.get(intQus).getOptions().get(0).getMyscale().length()];
            }
            mTableViewAdapter = new TableViewAdapter(mContext, mTableViewModel, 1);
        } else {
            if (multiselect == null) {
                multiselect = new String[questions.get(intQus).getOptions().get(0).getMyscale().length()]
                        [questions.get(intQus).getOptions().get(0).getMxscale().length()];
            }
            mTableViewAdapter = new TableViewAdapter(mContext, mTableViewModel, 0);
        }

        mTableView.setAdapter(mTableViewAdapter);
        JSONArray jaAns = null;
        if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
            try {
                jaAns = new JSONArray(answers[intQus]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mTableView.setTableViewListener(new TableViewListener(mTableView,
                mTableViewModel.getCellList(questions.
                                get(intQus).getOptions().get(0).getMyscale(),
                        questions.get(intQus).getOptions().get(0).getMxscale(), jaAns)));
        // Load the data to the TableView

        mTableViewAdapter.setAllItems(
                mTableViewModel.getColumnHeaderList(questions.get(intQus).getOptions().get(0).getMxscale()),
                mTableViewModel.getRowHeaderList(questions.get(intQus).getOptions().get(0).getMyscale()),
                mTableViewModel.getCellList(questions.get(intQus).getOptions().get(0).getMyscale(),
                        questions.get(intQus).getOptions().get(0).getMxscale(), jaAns));
    }
    //endregion

    //region createEditText
    @SuppressLint("NewApi")
    public void createTextView(final String ans_values) {
        mtxt_date_time = new TextView(mContext);
        mtxt_date_time.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mtxt_date_time.setTextSize(14);
        mtxt_date_time.setTextColor(getResources().getColor(R.color.colorPrimary));
        mtxt_date_time.setPadding(50, 10, 50, 10);
        mtxt_date_time.setBackgroundColor(R.drawable.text_view_bg);
        if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
            if (!answers[intQus].equalsIgnoreCase("")) {
                mtxt_date_time.setText(answers[intQus]);
            } else {
                if (ans_values.equalsIgnoreCase("1")) {
                    mtxt_date_time.setHint(R.string.select_date);
                } else if (ans_values.equalsIgnoreCase("2")) {
                    mtxt_date_time.setHint(R.string.select_time);
                } else if (ans_values.equalsIgnoreCase("3")) {
                    mtxt_date_time.setHint(R.string.select_date_time);
                }

            }
        } else {
            if (ans_values.equalsIgnoreCase("1")) {
                mtxt_date_time.setHint(R.string.select_date);
            } else if (ans_values.equalsIgnoreCase("2")) {
                mtxt_date_time.setHint(R.string.select_time);
            } else if (ans_values.equalsIgnoreCase("3")) {
                mtxt_date_time.setHint(R.string.select_date_time);
            }
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(50, 10, 50, 0);
        mtxt_date_time.setLayoutParams(layoutParams);
        mLlAnswer.addView(mtxt_date_time);

        mtxt_date_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                if (ans_values.equalsIgnoreCase("3")) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, final int year,
                                                      final int monthOfYear, final int dayOfMonth) {
                                    // Get Current Time
                                    final Calendar c = Calendar.getInstance();
                                    mHour = c.get(Calendar.HOUR_OF_DAY);
                                    mMinute = c.get(Calendar.MINUTE);

                                    // Launch Time Picker Dialog
                                    TimePickerDialog timePickerDialog = new TimePickerDialog(mContext,
                                            new TimePickerDialog.OnTimeSetListener() {

                                                @Override
                                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                                      int minute) {
                                                    String minuteofhours = String.valueOf(minute), hours = String.valueOf(hourOfDay);
                                                    if (String.valueOf(minute).length() == 1) {
                                                        minuteofhours = "0" + minute;
                                                    }
                                                    if (String.valueOf(hourOfDay).length() == 1) {
                                                        hours = "0" + hourOfDay;
                                                    }
                                                    mtxt_date_time.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year + " " + hours + " : " + minuteofhours);

                                                }
                                            }, mHour, mMinute, false);
                                    timePickerDialog.show();

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                } else if (ans_values.equalsIgnoreCase("1")) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, final int year,
                                                      final int monthOfYear, final int dayOfMonth) {
                                    mtxt_date_time.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);


                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                } else if (ans_values.equalsIgnoreCase("2")) {
                    // Get Current Time
                    c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);

                    // Launch Time Picker Dialog
                    TimePickerDialog timePickerDialog = new TimePickerDialog(mContext,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {
                                    String minuteofhours = String.valueOf(minute), hours = String.valueOf(hourOfDay);
                                    if (String.valueOf(minute).length() == 1) {
                                        minuteofhours = "0" + minute;
                                    }
                                    if (String.valueOf(hourOfDay).length() == 1) {
                                        hours = "0" + hourOfDay;
                                    }
                                    mtxt_date_time.setText(hours + " : " + minuteofhours);
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();
                }
            }
        });
    }
    //endregion

    //region For MultiSpinner
    public void createMultiSpinner() {
        mLlAnswer.setVisibility(View.GONE);
        mTableView.setVisibility(View.GONE);
        msimpleMultiSpinner.setVisibility(View.VISIBLE);
        strOptionMultiSpinnervalue = "";
        final LinkedHashMap<String, Boolean> items = new LinkedHashMap<>();

        items.put("Select", false);
        for (int i = 0; i < questions.get(intQus).getOptions().size(); i++) {
            items.put(questions.get(intQus).getOptions().get(i).getMtext(), false);
        }

        if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(answers[intQus]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < items.size(); i++) {
                Set<Map.Entry<String, Boolean>> mapSet = items.entrySet();
                Map.Entry<String, Boolean> elementAt = (new ArrayList<Map.Entry<String, Boolean>>(mapSet)).get(i);
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int j = 0; j < jsonArray.length(); j++) {
                        try {
                            if (jsonArray.get(j).toString().equalsIgnoreCase(elementAt.getKey())) {
                                items.put(String.valueOf(jsonArray.get(j)), true);
                                msimpleMultiSpinner.setItems(items);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        msimpleMultiSpinner.setItems(items, new MultiSpinnerListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemsSelected(boolean[] selected, List item) {
                for (int i = 0; i < selected.length; i++) {
                    if (selected[i]) {
                        for (int j = 0; j < item.size(); j++) {
                            jaMultiSelection.put(item.get(j));
                        }
                    } else {
                        jaMultiSelection.remove(i);
                    }
                }
            }

        });
    }
    //endregion

    // region For MultiSpinner
    public void createMultiSpinner1() {
        final TextView textView = new TextView(this);
        textView.setTextColor(Color.parseColor("#000000"));
        textView.setText("Select Answer");
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(50, 10, 10, 0);
        textView.setLayoutParams(layoutParams);

        if (questions.get(intQus).getOptions().size() > 0) {
            list = new String[questions.get(intQus).getOptions().size()];
            for (int i = 0; i < questions.get(intQus).getOptions().size(); i++) {
                list[i] = String.valueOf(questions.get(intQus).getOptions().get(i).getMtext());
            }
        }
        checkedList = new boolean[list.length];

        if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(answers[intQus]);
                String string = "";
                for (int i = 0; i < jsonArray.length(); i++) {
                    for (int j = 0; j < integers.size(); j++) {
                        try {
                            if (jsonArray.get(i).toString().equalsIgnoreCase(list[integers.get(j)])) {
                                checkedList[integers.get(j)] = true;
                                string = string + list[integers.get(j)];
                               /* if (jsonArray.get(i).equals(list[integers.get(j)])) {

                                }*/
                                if (i != integers.size() - 1) {
                                    string = string + " , ";
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                textView.setText(string);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder aBuilder = new AlertDialog.Builder(mContext);
                aBuilder.setTitle("Select Answer");
                aBuilder.setMultiChoiceItems(list, checkedList, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which, boolean b) {
                        if (b) {
                            if (!integers.contains(which)) {
                                integers.add(which);
                            }
                        } else {
                            String string = "";
                            for (int i = 0; i < integers.size(); i++) {
                                if (integers.get(i) == which) {
                                    integers.remove(i);
                                }
                                for (int j = 0; j < integers.size(); j++) {
                                    jaMultiSelection.put(list[integers.get(j)]);
                                    string = string + list[integers.get(j)];
                                    if (i != integers.size() - 1) {
                                        string = string + ", ";
                                    }
                                }
                            }
                            if (!string.equals("")) {
                                textView.setText(string);
                            } else {
                                textView.setText("Select Answer");
                            }

                        }
                    }
                });
                aBuilder.setCancelable(false);
                aBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        String string = "";
                        jaMultiSelection = new JSONArray();
                        for (int i = 0; i < integers.size(); i++) {
                            jaMultiSelection.put(list[integers.get(i)]);
                            string = string + list[integers.get(i)];
                            if (i != integers.size() - 1) {
                                string = string + " , ";
                            }
                        }
                        if (string.equalsIgnoreCase("")) {
                            textView.setText("Select Answer");
                        } else {
                            textView.setText(string);
                        }

                    }
                });
                aBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                aBuilder.setNeutralButton("Clear", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        dialogInterface.dismiss();
                        for (int i = 0; i < checkedList.length; i++) {
                            checkedList[i] = false;
                            integers.clear();
                            textView.setText("Select Answer");

                        }
                    }
                });

                AlertDialog dialog = aBuilder.create();
                dialog.show();
            }
        });
        mLlAnswer.addView(textView);
    }
    //endregion

    //region For create dropdown
    public void createDropdown() {
        strOptionSpinervalue = "";
        Spinner spinner = new Spinner(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(50, 0, 0, 0);
        spinner.setLayoutParams(params);
        final ArrayList<String> stringsArray = new ArrayList<>();
        stringsArray.add("Select");
        for (int i = 0; i < questions.get(intQus).getOptions().size(); i++) {
            stringsArray.add(questions.get(intQus).getOptions().get(i).getMtext());
        }


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, stringsArray);
        spinner.setAdapter(spinnerArrayAdapter);
        mLlAnswer.addView(spinner);

        if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
            for (int i = 0; i < stringsArray.size(); i++) {
                if (answers[intQus].equalsIgnoreCase(stringsArray.get(i))) {
                    spinner.setSelection(i);
                }
            }
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(MainActivity.this, getString(R.string.selected_item) + " " + personNames[position], Toast.LENGTH_SHORT).show();
                strOptionSpinervalue = stringsArray.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }
    //endregion

    //region createMultiLineEditText
    @SuppressLint("NewApi")
    public void createMultiLineEditText() {
        mEdtMutiLine = new EditText(mContext);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(50, 0, 50, 0);
        mEdtMutiLine.setLayoutParams(layoutParams);
        mEdtMutiLine.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        mEdtMutiLine.setSingleLine(false);
        mEdtMutiLine.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        mEdtMutiLine.setMinLines(4);
        mEdtMutiLine.setLines(4);
        mEdtMutiLine.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mEdtMutiLine.setTextSize(14);
        mEdtMutiLine.setGravity(Gravity.TOP);
        mEdtMutiLine.setPadding(10, 10, 10, 10);
        mEdtMutiLine.setTextColor(getResources().getColor(R.color.colorPrimary));
        mEdtMutiLine.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        mEdtMutiLine.setVerticalScrollBarEnabled(true);

        if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
            if (!answers[intQus].equalsIgnoreCase("")) {
                mEdtMutiLine.setText(answers[intQus]);
            }
        }
        mLlAnswer.addView(mEdtMutiLine);

    }//endregion

    //region createEditText
    @SuppressLint("NewApi")
    public void createEditText() {
        mEdtText = new EditText(mContext);
        mEdtText.setInputType(InputType.TYPE_CLASS_TEXT);
        mEdtText.setTypeface(Typefaces.Ubuntu_Regular(mContext));
        mEdtText.setTextSize(14);
        mEdtText.setTextColor(getResources().getColor(R.color.colorPrimary));
        mEdtText.setPadding(10, 10, 10, 10);
        if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
            if (!answers[intQus].equalsIgnoreCase("")) {
                mEdtText.setText(answers[intQus]);
            }
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                110);
        layoutParams.setMargins(50, 0, 50, 10);
        mEdtText.setLayoutParams(layoutParams);
        mLlAnswer.addView(mEdtText);
    }//endregion

    //region createRadio
    public void createRadio() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                110);
        layoutParams.setMargins(50, 0, 50, 10);
        RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setLayoutParams(layoutParams);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        RadioOptionAdapter radioOptionAdapter = null;
        if (questions.get(intQus).getOptions() != null && questions.get(intQus).getOptions().size() > 0) {
            if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(answers[intQus]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                strOptionRadioTextValue = jsonObject.optString("text");
                strOptionRadioImageValue = jsonObject.optString("image");
                radioOptionAdapter = new RadioOptionAdapter(mContext, questions.get(intQus).getOptions(), strOptionRadioTextValue, strOptionRadioImageValue);
            } else {
                strOptionRadioTextValue = "";
                strOptionRadioImageValue = "";
                radioOptionAdapter = new RadioOptionAdapter(mContext, questions.get(intQus).getOptions(), strOptionRadioTextValue, strOptionRadioImageValue);
            }
            recyclerView.setAdapter(radioOptionAdapter);
        }
        mLlAnswer.addView(recyclerView);
    }//endregion

    // region createCheckBox
    public void createCheckBox() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(50, 0, 50, 10);
        RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setLayoutParams(layoutParams);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        CheckboxOptionAdapter checkboxOptionAdapter = null;
        if (questions.get(intQus).getOptions() != null && questions.get(intQus).getOptions().size() > 0) {
            if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
                try {
                    jsonArrayCheckbox = new JSONArray(answers[intQus]);
                    checkboxOptionAdapter = new CheckboxOptionAdapter(mContext, questions.get(intQus).getOptions(), answers[intQus]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                answers[intQus] = "";
                jsonArrayCheckbox = new JSONArray();
                checkboxOptionAdapter = new CheckboxOptionAdapter(mContext, questions.get(intQus).getOptions(), "");
            }
            recyclerView.setAdapter(checkboxOptionAdapter);
        }
        mLlAnswer.addView(recyclerView);

    }//endregion

    //region createLinearLayout
    public void createLinearLayoutHORIZONTAL(View view) {
        LinearLayout layout = new LinearLayout(mContext);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        layout.setPadding(10, 10, 5, 5);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(10, 0, 10, 0);
        layout.setLayoutParams(layoutParams);
        layout.setWeightSum(2f);
        layout.addView(view);
        mLlAnswer.addView(layout);
    }//endregion

    // region createLinearLayout
    public void createLinearLayoutVERTICAL(View view) {
        LinearLayout layout = new LinearLayout(mContext);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setPadding(10, 10, 5, 5);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
                (0,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.weight = 1.0f;
        layoutParams.setMargins(10, 0, 10, 0);
        layout.setLayoutParams(layoutParams);
        layout.addView(view);
        createLinearLayoutHORIZONTAL(layout);
    }//endregion

    //region createImageView
    @SuppressLint("NewApi")
    public void createImageView() {
        String[] strings1 =
                {"https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/05/diamond-bracelet/1525167957_771145001509366348_cover_imageB-451 P_1024x1024.jpg",
                        "https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/04/amour-bracelet/1525078305_723149001509364703_cover_imageB-451_P_1024x1024.jpg",
                        "https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/04/elite-solitaire-bridal-ring/1525078058_757931001509360035_cover_imageDOC R-6 FW_1024x1024.jpg",
                        "https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/04/dazzle-bridal-ring/1525076359_249097001509360481_cover_imageDOC%20R-6%20FY_1024x1024.jpg"};
        for (int i = 0; i < 2; i++) {
            LinearLayout layout = new LinearLayout(mContext);
            layout.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(10, 0, 10, 0);
            layout.setLayoutParams(layoutParams);
            layout.setWeightSum(2f);
            for (int j = 0; j < 2; j++) {
                LinearLayout layoutVertical = new LinearLayout(mContext);
                layoutVertical.setOrientation(LinearLayout.VERTICAL);
                LinearLayout.LayoutParams layoutVertiaclParams = new LinearLayout.LayoutParams
                        (LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);

                layoutVertiaclParams.weight = 1.0f;
                ImageView imageView = new ImageView(mContext);
                LinearLayout.LayoutParams imageLayoutParams = new LinearLayout.LayoutParams
                        (100, 100);
                imageLayoutParams.setMargins(10, 10, 10, 0);
                imageView.setLayoutParams(imageLayoutParams);
                /*imageView.setImageResource(R.drawable.ic_login_top_bg);*/

                //region Image
                String picUrl = null;
                try {
                    URL urla = null;
                    urla = new URL(strings1[j].replaceAll("%20", " "));
                    URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(), urla.getPath(), urla.getQuery(), urla.getRef());
                    picUrl = String.valueOf(urin.toURL());
                    // Capture position and set to the ImageView
                    Picasso.with(mContext)
                            .load(picUrl)
                            .into(imageView, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {

                                }
                            });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                   /* SendMail sm = new SendMail(mContext, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();*/
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    //Creating SendMail object
                   /* SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in ViewPagerAdapter.java When parsing url\n" + e.toString());
                    //Executing sendmail to send email
                    sm.execute();*/
                }
                //endregion

                layoutVertical.addView(imageView);
                layout.addView(layoutVertical);
            }
            mLlAnswer.addView(layout);
        }

    }//endregion

    //region CreateImageViewRecyclerView
    public void CreateImageViewRecyclerView() {
        String[] strings =
                {"https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/05/diamond-bracelet/1525167957_771145001509366348_cover_imageB-451 P_1024x1024.jpg",
                        "https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/04/amour-bracelet/1525078305_723149001509364703_cover_imageB-451_P_1024x1024.jpg",
                        "https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/04/elite-solitaire-bridal-ring/1525078058_757931001509360035_cover_imageDOC R-6 FW_1024x1024.jpg",
                        "https://js-joel-kart.s3.ap-south-1.amazonaws.com/images/2018/04/dazzle-bridal-ring/1525076359_249097001509360481_cover_imageDOC%20R-6%20FY_1024x1024.jpg"};
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                110);
        layoutParams.setMargins(50, 0, 50, 10);
        RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setLayoutParams(layoutParams);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        ImageOptionAdapter imageOptionAdapter = null;
        if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
            strOptionImageValue = answers[intQus];
            imageOptionAdapter = new ImageOptionAdapter(mContext, strings, strOptionImageValue);
        } else {
            strOptionImageValue = "";
            imageOptionAdapter = new ImageOptionAdapter(mContext, strings, strOptionImageValue);
        }

       /* if (answers1.get(intQus) != null) {
            strOptionImageValue = answers1.get(intQus).toString();
            imageOptionAdapter = new ImageOptionAdapter(mContext, strings, strOptionImageValue);
        } else {
            strOptionImageValue = "";
            imageOptionAdapter = new ImageOptionAdapter(mContext, strings, strOptionImageValue);
        }*/
        recyclerView.setAdapter(imageOptionAdapter);
        mLlAnswer.addView(recyclerView);
    }//endregion

    // region create Single CheckBox
    public void createSingleCheckBox() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                110);
        layoutParams.setMargins(50, 0, 50, 10);
        CheckBox checkBox = new CheckBox(this);
        checkBox.setLayoutParams(layoutParams);
        checkBox.setText(questions.get(intQus).getAns_display_type());
        if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
            if (!answers[intQus].equals("")) {
                if (answers[intQus].equalsIgnoreCase("on")) {
                    checkBox.setChecked(true);
                } else {
                    checkBox.setChecked(false);
                }
            }
        }
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    strSingleCheckboxValue = "on";
                } else {
                    strSingleCheckboxValue = "off";
                }

            }
        });
        mLlAnswer.addView(checkBox);

    }//endregion

    // region createRatingbar
    public void createRatingbar() {
        RatingBar ratingBar = new RatingBar(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(50, 30, 50, 30);
        ratingBar.setLayoutParams(layoutParams);
        ratingBar.setStepSize(1);
        ratingBar.setNumStars(5);

        if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
            if (!answers[intQus].equals("")) {
                ratingBar.setRating(Float.parseFloat(answers[intQus]));
            }
        }

        /*LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(0).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);*/

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                strRatingValue = String.valueOf(Math.round(ratingBar.getRating()));
            }
        });

        mLlAnswer.addView(ratingBar);

    }//endregion

    //region createCaptcha
    public void createCaptcha(String strValue) {
        Button btnCaptcha = new Button(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(50,0,0,0);
        btnCaptcha.setText(strValue);
        btnCaptcha.setLayoutParams(layoutParams);
        btnCaptcha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SafetyNet.getClient(mContext).verifyWithRecaptcha(StaticUtility.SAFETY_NET_API_SITE_KEY).
                        addOnSuccessListener((Activity) mContext, new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
                            @Override
                            public void onSuccess(SafetyNetApi.RecaptchaTokenResponse response) {
                                if (!response.getTokenResult().isEmpty()) {
                                    verifyTokenOnServer(response.getTokenResult());
                                    /*APICalling(response.getTokenResult());*/
                                }
                            }
                        }).addOnFailureListener((Activity) mContext, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            Log.d(TAG, "Error message: " +
                                    CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                        } else {
                            Log.d(TAG, "Unknown type of error: " + e.getMessage());
                        }
                    }
                });
            }
        });
        mLlAnswer.addView(btnCaptcha);
    }//endregion

    // region createSmileyRating
    @SuppressLint("WrongConstant")
    public void createSmileyRating() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                110);
        layoutParams.setMargins(50, 0, 50, 10);
        SmileRating smileRating = new SmileRating(this);
        smileRating.setLayoutParams(layoutParams);
        smileRating.setShowLine(false);

        if (!strSmileyRatingValue.equals("")) {
            switch (Integer.parseInt(strSmileyRatingValue)) {
                case 1:
                    smileRating.setSelectedSmile(BaseRating.TERRIBLE);
                    break;
                case 2:
                    smileRating.setSelectedSmile(BaseRating.BAD);
                    break;
                case 3:
                    smileRating.setSelectedSmile(BaseRating.OKAY);
                    break;
                case 4:
                    smileRating.setSelectedSmile(BaseRating.GOOD);
                    break;
                case 5:
                    smileRating.setSelectedSmile(BaseRating.GREAT);
                    break;
                default:
                    break;
            }

        }
        smileRating.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                strSmileyRatingValue = String.valueOf(level);
            }
        });
        mLlAnswer.addView(smileRating);
    }//endregion

    //region create FileUpload
    public void createFileUpload(String strValue) {
        Button btnFileUpload = new Button(this);
        mTxtFileUpload = new TextView(this);
        mTxtFileUpload.setPadding(10, 0, 0, 0);
        if (answers[intQus] != null && !answers[intQus].equalsIgnoreCase("")) {
            try {
                JSONObject object = new JSONObject(answers[intQus]);
                Iterator<String> iter = object.keys();
                while (iter.hasNext()) {
                    strFileDisplayName = iter.next();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mTxtFileUpload.setText(strFileDisplayName);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(50, 0, 0, 0);
        btnFileUpload.setText("Upload File");
        btnFileUpload.setLayoutParams(layoutParams);
        mTxtFileUpload.setLayoutParams(layoutParams);
        btnFileUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                        equalsIgnoreCase("gif")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                            ActivityCompat.requestPermissions(QuestionnaireScreen.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.setType("image/gif");
                            startActivityForResult(intent, PICKFILE_REQUEST_CODE);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("image/gif");
                        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
                    }
                } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                        equalsIgnoreCase("pdf")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                            ActivityCompat.requestPermissions(QuestionnaireScreen.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.setType("application/pdf");
                            startActivityForResult(intent, PICKFILE_REQUEST_CODE);
                        }
                    } else {
                        /*String[] mimeTypes = {"file/*"};
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("file/*");
                        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                        startActivityForResult(intent, PICKFILE_REQUEST_CODE);*/
                        Intent intent = new Intent();
                        //sets the select file to all types of files
                        intent.setType("*/*");
                        //allows to select data and return it
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        //starts new activity to select file and return data
                        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), PICKFILE_REQUEST_CODE);
                    }
                } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                        equalsIgnoreCase("jpg")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                            ActivityCompat.requestPermissions(QuestionnaireScreen.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                        } else {
                            String[] mimeTypes = {"image/jpeg", "image/jpg"};
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                            startActivityForResult(intent, PICKFILE_REQUEST_CODE);
                        }
                    } else {
                        String[] mimeTypes = {"image/jpeg", "image/jpg"};
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
                    }
                } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                        equalsIgnoreCase("png")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                            ActivityCompat.requestPermissions(QuestionnaireScreen.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.setType("image/png");
                            startActivityForResult(intent, PICKFILE_REQUEST_CODE);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("image/png");
                        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
                    }
                }
            }
        });
        mLlAnswer.addView(btnFileUpload);
        mLlAnswer.addView(mTxtFileUpload);
    }//endregion

    //region Validate
    private boolean Validate() {
        if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("text")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (!TextUtils.isEmpty(mEdtText.getText().toString())) {
                    answers[intQus] = mEdtText.getText().toString();
                    return true;
                } else {
                    Toast.makeText(mContext, "Enter Answer...!", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (mEdtText.getText().length() > 0) {
                    answers[intQus] = mEdtText.getText().toString();
                } else {
                    answers[intQus] = "";
                }
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Textarea")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (mEdtMutiLine != null) {
                    if (!TextUtils.isEmpty(mEdtMutiLine.getText().toString())) {
                        answers[intQus] = mEdtMutiLine.getText().toString();
                        return true;
                    } else {
                        Toast.makeText(mContext, "Enter Answer...!", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                if (mEdtMutiLine.getText().length() > 0) {
                    answers[intQus] = mEdtMutiLine.getText().toString();
                } else {
                    answers[intQus] = "";
                }
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("radio")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (!strOptionRadioTextValue.equalsIgnoreCase("") || !strOptionRadioImageValue.equalsIgnoreCase("")) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("image", strOptionRadioImageValue);
                        jsonObject.put("text", strOptionRadioTextValue);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    answers[intQus] = jsonObject.toString();
                    return true;
                } else {
                    Toast.makeText(mContext, "Select Any Answer...!", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (!strOptionRadioImageValue.equals("") || !strOptionRadioTextValue.equals("")) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("image", strOptionRadioImageValue);
                        jsonObject.put("text", strOptionRadioTextValue);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    answers[intQus] = jsonObject.toString();
                } else {
                    answers[intQus] = "";
                }
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("checkbox")) {
            jsonArrayCheckbox = CheckboxOptionAdapter.jsonArray;
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (jsonArrayCheckbox != null && jsonArrayCheckbox.length() > 0) {
                    answers[intQus] = jsonArrayCheckbox.toString();
                    return true;
                } else {
                    answers[intQus] = "";
                    Toast.makeText(mContext, "Select Any Answer...!", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (jsonArrayCheckbox != null && jsonArrayCheckbox.length() > 0) {
                    answers[intQus] = jsonArrayCheckbox.toString();
                } else {
                    answers[intQus] = "";
                }
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("image")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (!strOptionImageValue.equalsIgnoreCase("")) {
                    answers[intQus] = strOptionImageValue;
                    return true;
                } else {
                    Toast.makeText(mContext, "Select Any Answer...!", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (!strOptionImageValue.equalsIgnoreCase("")) {
                    answers[intQus] = strOptionImageValue;
                } else {
                    answers[intQus] = "";
                }
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Dropdown")) {
            if (!strOptionSpinervalue.equalsIgnoreCase("Select")) {
                answers[intQus] = strOptionSpinervalue;
                return true;
            } else {
                if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                    Toast.makeText(mContext, "Select Any Answer...!", Toast.LENGTH_SHORT).show();
                } else {
                    answers[intQus] = "";
                    return true;
                }
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Multi Select")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (jaMultiSelection.length() > 0) {
                    /*StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < jaMultiSelection.length(); i++) {
                        stringBuilder.append(jaMultiSelection.opt(i));
                        if (jaMultiSelection.length() != i + 1) {
                            stringBuilder.append(",");
                        }
                    }
                    answers[intQus] = stringBuilder.toString();*/
                    answers[intQus] = jaMultiSelection.toString();
                    return true;
                } else {
                    Toast.makeText(mContext, "Select Any Answer...!", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (jaMultiSelection.length() > 0) {
                    /*StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < jaMultiSelection.length(); i++) {
                        stringBuilder.append(jaMultiSelection.opt(i));
                        if (jaMultiSelection.length() != i + 1) {
                            stringBuilder.append(",");
                        }
                    }
                    answers[intQus] = stringBuilder.toString();*/
                    answers[intQus] = jaMultiSelection.toString();
                } else {
                    answers[intQus] = "";
                }
                return true;
            }

        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Date/Time")) {
            if (!TextUtils.isEmpty(mtxt_date_time.getText().toString())) {
                answers[intQus] = mtxt_date_time.getText().toString();
                return true;
            } else {
                if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                    if (questions.get(intQus).getAns_values().equalsIgnoreCase("1")) {
                        Toast.makeText(mContext, getResources().getString(R.string.select_date), Toast.LENGTH_SHORT).show();
                    } else if (questions.get(intQus).getAns_values().equalsIgnoreCase("2")) {
                        Toast.makeText(mContext, getResources().getString(R.string.select_time), Toast.LENGTH_SHORT).show();
                    } else if (questions.get(intQus).getAns_values().equalsIgnoreCase("3")) {
                        Toast.makeText(mContext, getResources().getString(R.string.select_date_time), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    answers[intQus] = "";
                    return true;
                }
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Multi-Point Scales")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                boolean is_answer = true;
                for (int i = 0; i < questions.get(intQus).getOptions().get(0).getMyscale().length(); i++) {
                    if (multipoint[i] == null) {
                        is_answer = false;
                        break;
                    }
                }
                if (is_answer && multipoint.length == questions.get(intQus).getOptions().get(0).getMyscale().length()) {
                    JSONArray jsonArray = new JSONArray();
                    JSONObject joMultiScale = new JSONObject();
                    for (int i = 0; i < multipoint.length; i++) {
                        try {
                            joMultiScale.put(String.valueOf(questions.get(intQus).getOptions().get(0).getMyscale().get(i)), multipoint[i]);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    jsonArray.put(joMultiScale);
                    answers[intQus] = jsonArray.toString();
                    return true;
                } else {
                    Toast.makeText(mContext, "Select multiple option...!", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (multipoint[0] != null) {
                    if (multipoint.length == questions.get(intQus).getOptions().get(0).getMyscale().length()) {
                        JSONArray jsonArray = new JSONArray();
                        JSONObject joMultiScale = new JSONObject();
                        for (int i = 0; i < multipoint.length; i++) {
                            try {
                                joMultiScale.put(String.valueOf(questions.get(intQus).getOptions().get(0).getMyscale().get(i)), multipoint[i]);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        jsonArray.put(joMultiScale);
                        answers[intQus] = jsonArray.toString();
                    }
                } else {
                    answers[intQus] = "";
                }
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Multi-Select")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (multiselect.length == questions.get(intQus).getOptions().get(0).getMyscale().length()) {
                    JSONArray mainjsonArray = new JSONArray();
                    JSONObject joMultiSelect = new JSONObject();
                    for (int i = 0; i < multiselect.length; i++) {
                        JSONArray jsonArray = new JSONArray();
                        for (int j = 0; j < multiselect[i].length; j++) {
                            if (multiselect[i][j] != null) {
                                try {
                                    joMultiSelect.put(String.valueOf(questions.get(intQus).getOptions().get(0).getMyscale().get(i)), jsonArray.put(multiselect[i][j]));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    mainjsonArray.put(joMultiSelect);
                    answers[intQus] = mainjsonArray.toString();
                    return true;
                } else {
                    Toast.makeText(mContext, "Select multiple option...!", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (multiselect != null && multiselect[0] != null) {
                    if (multiselect.length == questions.get(intQus).getOptions().get(0).getMyscale().length()) {
                        JSONArray mainjsonArray = new JSONArray();
                        JSONObject joMultiSelect = new JSONObject();
                        for (int i = 0; i < multiselect.length; i++) {
                            JSONArray jsonArray = new JSONArray();
                            for (int j = 0; j < multiselect[i].length; j++) {
                                if (multiselect[i][j] != null) {
                                    try {
                                        joMultiSelect.put(String.valueOf(questions.get(intQus).getOptions().get(0).getMyscale().get(i)), jsonArray.put(multiselect[i][j]));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                        mainjsonArray.put(joMultiSelect);
                        answers[intQus] = mainjsonArray.toString();
                    }
                } else {
                    answers[intQus] = "";
                }
                return true;
            }

        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Rank Order")) {
            if (rankOrder.length == questions.get(intQus).getOptions().size()) {
                JSONArray mainjsonArray = new JSONArray();
                for (int i = 0; i < rankOrder.length; i++) {
                    if (rankOrder[i] != null) {
                        mainjsonArray.put(rankOrder[i]);
                    }
                }
                answers[intQus] = mainjsonArray.toString();
                return true;
            } else {
                if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                    Toast.makeText(mContext, "Select multiple option...!", Toast.LENGTH_SHORT).show();
                } else {
                    answers[intQus] = "";
                    return true;
                }
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Captcha")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (!strCaptchaStatusValue.equals("")) {
                    answers[intQus] = strCaptchaStatusValue;
                    return true;
                } else {
                    Toast.makeText(mContext, "Captcha Required...!", Toast.LENGTH_SHORT).show();
                }
            } else {
                answers[intQus] = strCaptchaStatusValue;
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Terms & Condition")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (!strSingleCheckboxValue.equalsIgnoreCase("")) {
                    if (strSingleCheckboxValue.equalsIgnoreCase("on")) {
                        answers[intQus] = strSingleCheckboxValue;
                        return true;
                    } else {
                        Toast.makeText(mContext, "Accept term and condition...!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, "Accept term and condition...!", Toast.LENGTH_SHORT).show();
                }
            } else {
                answers[intQus] = strSingleCheckboxValue;
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Rating")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (!strRatingValue.equals("")) {
                    answers[intQus] = strRatingValue;
                    return true;
                } else {
                    Toast.makeText(mContext, "Please give rating...!", Toast.LENGTH_SHORT).show();
                }
            } else {
                answers[intQus] = strRatingValue;
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Smiley")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (!strSmileyRatingValue.equals("")) {
                    answers[intQus] = strSmileyRatingValue;
                    return true;
                } else {
                    Toast.makeText(mContext, "Please give rating...!", Toast.LENGTH_SHORT).show();
                }
            } else {
                answers[intQus] = strSmileyRatingValue;
                return true;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("File Upload")) {
            if (questions.get(intQus).getIs_mandatory().equalsIgnoreCase("1")) {
                if (!strFileDisplayName.equals("") && !strFileUploadValue.equals("")) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(strFileDisplayName, strFileUploadValue);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    answers[intQus] = jsonObject.toString();
                    return true;
                } else {
                    Toast.makeText(mContext, "Select File...!", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (!strFileDisplayName.equals("") && !strFileUploadValue.equals("")) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(strFileDisplayName, strFileUploadValue);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    answers[intQus] = jsonObject.toString();
                } else {
                    answers[intQus] = "";
                }

                return true;
            }
        }
        return false;
    }
    //endregion

    //region For add answer
    private boolean addanswer() {
        if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("text")) {
            if (!TextUtils.isEmpty(mEdtText.getText().toString())) {
                answers[intQus] = mEdtText.getText().toString();
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Textarea")) {
            if (mEdtMutiLine != null) {
                if (!TextUtils.isEmpty(mEdtMutiLine.getText().toString())) {
                    answers[intQus] = mEdtMutiLine.getText().toString();

                }
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("radio")) {
            if (!strOptionRadioTextValue.equalsIgnoreCase("") || !strOptionRadioImageValue.equalsIgnoreCase("")) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("image", strOptionRadioImageValue);
                    jsonObject.put("text", strOptionRadioTextValue);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                answers[intQus] = jsonObject.toString();
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("checkbox")) {
            jsonArrayCheckbox = CheckboxOptionAdapter.jsonArray;
            if (jsonArrayCheckbox != null && jsonArrayCheckbox.length() > 0) {
                answers[intQus] = jsonArrayCheckbox.toString();

            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("image")) {
            if (!strOptionImageValue.equalsIgnoreCase("")) {
                answers[intQus] = strOptionImageValue;
            }

        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Dropdown")) {
            if (!strOptionSpinervalue.equalsIgnoreCase("Select")) {
                answers[intQus] = strOptionSpinervalue;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Multi Select")) {

            if (jaMultiSelection.length() > 0) {
                answers[intQus] = jaMultiSelection.toString();

            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Date/Time")) {
            if (!TextUtils.isEmpty(mtxt_date_time.getText().toString())) {
                answers[intQus] = mtxt_date_time.getText().toString();
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Multi-Point Scales")) {
            boolean is_answer = true;
            for (int i = 0; i < questions.get(intQus).getOptions().get(0).getMyscale().length(); i++) {
                if (multipoint[i] == null) {
                    is_answer = false;
                    break;
                }
            }
            if (is_answer && multipoint.length == questions.get(intQus).getOptions().get(0).getMyscale().length()) {
                JSONArray jsonArray = new JSONArray();
                JSONObject joMultiScale = new JSONObject();
                for (int i = 0; i < multipoint.length; i++) {
                    try {
                        joMultiScale.put(String.valueOf(questions.get(intQus).getOptions().get(0).getMyscale().get(i)), multipoint[i]);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                jsonArray.put(joMultiScale);
                answers[intQus] = jsonArray.toString();
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Multi-Select")) {
            if (multiselect.length == questions.get(intQus).getOptions().get(0).getMyscale().length()) {
                JSONArray mainjsonArray = new JSONArray();
                JSONObject joMultiSelect = new JSONObject();
                for (int i = 0; i < multiselect.length; i++) {
                    JSONArray jsonArray = new JSONArray();
                    for (int j = 0; j < multiselect[i].length; j++) {
                        if (multiselect[i][j] != null) {
                            try {
                                joMultiSelect.put(String.valueOf(questions.get(intQus).getOptions().get(0).getMyscale().get(i)), jsonArray.put(multiselect[i][j]));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                mainjsonArray.put(joMultiSelect);
                answers[intQus] = mainjsonArray.toString();
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Rank Order")) {
            if (rankOrder.length == questions.get(intQus).getOptions().size()) {
                JSONArray mainjsonArray = new JSONArray();
                for (int i = 0; i < rankOrder.length; i++) {
                    if (rankOrder[i] != null) {
                        mainjsonArray.put(rankOrder[i]);
                    }
                }
                answers[intQus] = mainjsonArray.toString();
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Captcha")) {
            if (!strCaptchaStatusValue.equals("")) {
                answers[intQus] = strCaptchaStatusValue;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Terms & Condition")) {
            if (!strSingleCheckboxValue.equalsIgnoreCase("")) {
                if (strSingleCheckboxValue.equalsIgnoreCase("on")) {
                    answers[intQus] = strSingleCheckboxValue;
                }
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Rating")) {
            if (!strRatingValue.equals("")) {
                answers[intQus] = strRatingValue;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("Smiley")) {
            if (!strSmileyRatingValue.equals("")) {
                answers[intQus] = strSmileyRatingValue;
            }
        } else if (questions.get(intQus).getAns_display_type().equalsIgnoreCase("File Upload")) {
            if (!strFileDisplayName.equals("") && !strFileUploadValue.equals("")) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(strFileDisplayName, strFileUploadValue);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                answers[intQus] = jsonObject.toString();
            }
        }
        return true;
    }
    //endregion

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                    equalsIgnoreCase("png")) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/png");
                startActivityForResult(intent, PICKFILE_REQUEST_CODE);
            } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                    equalsIgnoreCase("jpg")) {
                String[] mimeTypes = {"image/jpeg", "image/jpg"};
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                startActivityForResult(intent, PICKFILE_REQUEST_CODE);
            } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                    equalsIgnoreCase("pdf")) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("application/pdf");
                startActivityForResult(intent, PICKFILE_REQUEST_CODE);
            } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                    equalsIgnoreCase("gif")) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/gif");
                startActivityForResult(intent, PICKFILE_REQUEST_CODE);
            }
        } else {
            Toast.makeText(mContext, getResources().getString(R.string.permission_storage), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void SelectedAnswer(int Position, String Type) {
        if (Type.equalsIgnoreCase("1")) {
            strOptionImageValue = String.valueOf(Position);
        } else {
            strOptionImageValue = "";
        }
    }

    @Override
    public void SelectedRadioOption(int Position, String TextValue, String ImageValue) {
        strOptionRadioTextValue = TextValue;
        strOptionRadioImageValue = ImageValue;
    }

    //region verifyTokenOnServer
    public void verifyTokenOnServer(final String token) {
        Log.d(TAG, "Captcha Token" + token);

        StringRequest strReq = new StringRequest(Request.Method.POST,
                URL_VERIFY_ON_SERVER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean success = jsonObject.getBoolean("success");
                    /*String message = jsonObject.getString("message");*/
                    if (success) {
                        Toast.makeText(mContext, "Verified successfully...!", Toast.LENGTH_SHORT).show();
                        strCaptchaStatusValue = "1";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json Error:" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("secret", StaticUtility.SAFETY_NET_API_SECRET_KEY);
                params.put("response", token);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(strReq);
    }
    //endregion

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // do somthing...
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICKFILE_REQUEST_CODE) {
            Bitmap bitmap = null;
            String encoded = "";
            if (data != null) {
                Uri mImageCaptureUri = data.getData();
                String path = getRealPathFromURI(mImageCaptureUri); // From Gallery

                if (path == null) {
                    try {
                        path = FilePath.getPath(this, mImageCaptureUri); // From File Manager
                    } catch (Exception e) {
                        e.printStackTrace();
                        path = mImageCaptureUri.getPath();
                    }
                }
                Cursor cursor = null;
                try {
                    cursor = this.getContentResolver().query(mImageCaptureUri, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        strFileDisplayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        String extension = MimeTypeMap.getFileExtensionFromUrl(strFileDisplayName);
                        if (extension != null) {
                            if (extension.equalsIgnoreCase("pdf") ||
                                    extension.equalsIgnoreCase("gif")) {
                                if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                        equalsIgnoreCase(extension)) {
                                    //String[] filepath = path.split(":");
                                    if (path != null && path.contains(":")) {
                                        path = path.split(":")[1];
                                    }
                                    File file = new File(path);
                                    String size = "";
                                    try {
                                        size = Global.getFileSize(file);
                                    } catch (IllegalArgumentException e) {
                                        e.printStackTrace();
                                    }
                                    if (size.contains("KB")) {
                                        size = size.replace("KB", "");
                                        if (Double.parseDouble(questions.get(intQus).getOptions().get(0).getMobject().
                                                optString("size_limit")) >= Double.parseDouble(size)) {
                                            try {
                                                encoded = encodeFileToBase64Binary(file);
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            if (extension.equalsIgnoreCase("gif")) {
                                                strFileUploadValue = "data:image/" + extension + ";base64," + encoded;
                                            } else {
                                                strFileUploadValue = "data:application/" + extension + ";base64," + encoded;
                                            }
                                            mTxtFileUpload.setText(strFileDisplayName);
                                        } else {
                                            Toast.makeText(mContext, "Exceeding maximum allowed file size!", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(mContext, "Exceeding maximum allowed file size!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                            equalsIgnoreCase("pdf")) {
                                        Toast.makeText(mContext, "Select only pdf file...!", Toast.LENGTH_SHORT).show();
                                    } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                            equalsIgnoreCase("gif")) {
                                        Toast.makeText(mContext, "Select only gif file...!", Toast.LENGTH_SHORT).show();
                                    } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                            equalsIgnoreCase("jpg")) {
                                        Toast.makeText(mContext, "Select only jpg file...!", Toast.LENGTH_SHORT).show();
                                    } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                            equalsIgnoreCase("png")) {
                                        Toast.makeText(mContext, "Select only png file...!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } else if (extension.equalsIgnoreCase("png") ||
                                    extension.equalsIgnoreCase("jpeg") ||
                                    extension.equalsIgnoreCase("jpg")) {
                                if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                        equalsIgnoreCase(extension)) {
                                    if (path != null) {
                                        if (path.contains(":")) {
                                            path = path.split(":")[1];
                                        }
                                        bitmap = BitmapFactory.decodeFile(path);
                                        File file = new File(path);
                                        String size = Global.getFileSize(file);
                                        if (size.contains("KB")) {
                                            size = size.replace("KB", "");
                                            if (Double.parseDouble(questions.get(intQus).getOptions().get(0).getMobject().
                                                    optString("size_limit")) >= Double.parseDouble(size)) {
                                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                                                byte[] byteArray = byteArrayOutputStream.toByteArray();
                                                encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                                                strFileUploadValue = "data:image/" + extension + ";base64," + encoded;
                                                mTxtFileUpload.setText(strFileDisplayName);
                                            } else {
                                                Toast.makeText(mContext, "Exceeding maximum allowed file size!", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(mContext, "Exceeding maximum allowed file size!", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                } else {
                                    if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                            equalsIgnoreCase("jpg")) {
                                        Toast.makeText(mContext, "Select only jpg file...!", Toast.LENGTH_SHORT).show();
                                    } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                            equalsIgnoreCase("png")) {
                                        Toast.makeText(mContext, "Select only png file...!", Toast.LENGTH_SHORT).show();
                                    } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                            equalsIgnoreCase("pdf")) {
                                        Toast.makeText(mContext, "Select only pdf file...!", Toast.LENGTH_SHORT).show();
                                    } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                            equalsIgnoreCase("gif")) {
                                        Toast.makeText(mContext, "Select only gif file...!", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                        equalsIgnoreCase("pdf")) {
                                    Toast.makeText(mContext, "Select only pdf file...!", Toast.LENGTH_SHORT).show();
                                } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                        equalsIgnoreCase("gif")) {
                                    Toast.makeText(mContext, "Select only gif file...!", Toast.LENGTH_SHORT).show();
                                } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                        equalsIgnoreCase("jpg")) {
                                    Toast.makeText(mContext, "Select only jpg file...!", Toast.LENGTH_SHORT).show();
                                } else if (questions.get(intQus).getOptions().get(0).getMobject().optString("select_format").
                                        equalsIgnoreCase("png")) {
                                    Toast.makeText(mContext, "Select only png file...!", Toast.LENGTH_SHORT).show();
                                }
                            }

                        }
                    }
                } finally {
                    cursor.close();
                }
            }
        }
    }

    private String encodeFileToBase64Binary(File file)
            throws IOException {

        byte[] bytes = loadFile(file);
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);

        if (cursor == null)
            return null;

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    /*public static String convertFileToByteArray(File f) {
        byte[] byteArray = null;
        try {
            InputStream inputStream = new FileInputStream(f);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024 * 11];
            int bytesRead = 0;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();

            Log.e("Byte array", ">" + byteArray);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }*/
}
