package com.androidbuts.multispinnerfilter;

import java.util.List;

public interface MultiSpinnerListener {
    void onItemsSelected(boolean[] selected, List<String> item);
}